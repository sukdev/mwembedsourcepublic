/**
* Handles access control preview code
*/
( function( mw, $ ) { "use strict";

var acPreview = function( embedPlayer ){
	/**
	 * Trigger an access control preview dialog
	 */
	function acEndPreview(){
		mw.log( 'KWidgetSupport:: acEndPreview >' );
        // SUK: when access controll communicate is displayed disable the GUI
        // this suppose to cover the case when live stream is still pushed and user click play
        // even though communicate is displayed stream could be played by simple click play
        embedPlayer.sendNotification('enableGui', { 'guiEnabled' : false } );
		$( embedPlayer ).trigger( 'KalturaSupport_FreePreviewEnd' );
		// Don't run normal onend action:
		mw.log( 'KWidgetSupport:: KalturaSupport_FreePreviewEnd set onDoneInterfaceFlag = false' );
		embedPlayer.onDoneInterfaceFlag = false;
		var closeAcMessage = function(){
			$( embedPlayer ).unbind( '.acpreview' );
			embedPlayer.layoutBuilder.closeMenuOverlay();
			embedPlayer.onClipDone();
		};
		// On change media reset acPreview binding
		$( embedPlayer ).bind( 'onChangeMedia.acpreview', closeAcMessage );
		// Display player dialog
		// TODO i8ln!!
		// TODO migrate to displayAlert call
		if( embedPlayer.getKalturaConfig('', 'disableAlerts' ) !== true ){
			embedPlayer.layoutBuilder.displayMenuOverlay(
				$('<div />').append(
					$('<h3 />').append( embedPlayer.getKalturaMsg('FREE_PREVIEW_END_TITLE') ),
					$('<span />').text( embedPlayer.getKalturaMsg('FREE_PREVIEW_END') ),
					$('<br />'),$('<br />'),
					$('<button />').attr({'type' : "button"})
					.addClass( "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" )
					.append(
						$('<span />').addClass( "ui-button-text" )
						.text( 'Ok' )
						.css('margin', '10')
					).click( closeAcMessage )
				), closeAcMessage
			);
		}
	};
	// clear out any old bindings:
	$(embedPlayer).unbind( '.acPreview' );

    var timeInterval;
	var ac  = embedPlayer.kalturaContextData;
	// Check for preview access control and add special onEnd binding:
	if( ac.isAdmin === false && ac.isSessionRestricted === true && ac.previewLength && ac.previewLength != -1 ){
		$( embedPlayer ).bind('postEnded.acPreview', function(){
			acEndPreview( embedPlayer );
		});
        // ------------------ SUK ------------------------
        // when using access control on Android or iOS embedPlayer.currentTime set wrong value - all the time 0
        // question what is the problem was asked on kaltura forum - waiting for response
        // for now hotFix for android and iOS by set timer and popup ac message
        // when free view time pass
        // -----------------------------------------------
        if(embedPlayer.isLive() && (mw.isAndroid() || mw.isIOS()))
        {
            if (typeof console != 'undefined' && console.log) {
                console.log("embedPlayer.isLive()");
                console.log(embedPlayer.isLive());
            }

            var prevLength = embedPlayer.isLive() ? ac.previewLength + 5 : ac.previewLength;
            if (typeof console != 'undefined' && console.log) {
                console.log(prevLength);
            }
            embedPlayer.addJsListener( 'playerPlayed', function(){
                timeInterval = setTimeout(function(){
                    embedPlayer.stop();
                    if (mw.isIOS()) {
                        embedPlayer.getPlayerElement().webkitExitFullScreen();
                    }

                    acEndPreview() }, ( prevLength * 1000))
            });
        }
        else {
            // sometimes content does not have a content end at ac preview end time:
            $(embedPlayer).bind('monitorEvent.acPreview', function () {
                if (embedPlayer.currentTime >= ac.previewLength) {
                    // Stop content and show preview dialog:
                    embedPlayer.stop();
                    if (mw.isIOS()) {
                        embedPlayer.getPlayerElement().webkitExitFullScreen();
                    }
                    acEndPreview(embedPlayer);
                }
            });
        }
	}
};

//Check for new Embed Player events:
mw.addKalturaConfCheck( function( embedPlayer, callback ){
	if( embedPlayer.kalturaContextData ){
		acPreview( embedPlayer );
	}
	callback();
});

})( window.mw, jQuery );