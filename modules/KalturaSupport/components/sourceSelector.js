( function( mw, $ ) {"use strict";

	mw.PluginManager.add( 'sourceSelector', mw.KBaseComponent.extend({

		defaultConfig: {
			"parent": "controlsContainer",
			"order": 61,
			"displayImportance": 'low',
			"align": "right",
			"showTooltip": true,
			"switchOnResize": false,
			"simpleFormat": true,
			"iconClass": "icon-cog",
            "displayMode": "bitrate", //'bitrate' – displays frame size ( default ), 'bitrate' – displays the bitrate, 'sizebitrate' displays size followed by bitrate, 'sizefrombitrate' display size calculated from bitrate
            "hideSource": false,
			"title": gM( 'mwe-embedplayer-select_source' ),
			'smartContainer': 'qualitySettings',
			'smartContainerCloseEvent': 'SourceChange'
		},

		isDisabled: false,
		inUpdateLayout:false,
		selectSourceTitle: gM( 'mwe-embedplayer-select_source' ),
		switchSourceTitle: gM( 'mwe-embedplayer-switch_source' ),
        AutoTitle: gM( 'mwe-embedplayer-auto_source' ),
		saveBackgroundColor: null, // used to save background color upon disable and rotate and return it when enabled again to prevent rotating box around the icon when custom style is applied

        sourcesList: [],
        firstPlay:false,
        autoAdded:false,
        autoNeedSet:true,

		setup: function(){
			var _this = this;

			this.bind( 'playerReady sourcesReplaced', function(){
				_this.buildMenu();
			});

            // this.getPlayer().addJsListener("bytesTotalChange",function(){
            //     //--------------------- SUK ----------------------------
            //     // in case auto selection is available renegotiate the bitrate after video starts
            //     // used in bytesTotalChange because onPlay is only status saying that play signal was sent
            //     // which doesn't say that the stream is actually being play (important for slow connection 2G-250kb/s)
            //     if(_this.autoAdded && _this.autoNeedSet){
            //         setTimeout(function(){
            //             // dont force autobitrate to iOS devices - they have their own logic for abswitching
            //             if(!mw.isIOS()){
            //                 _this.getPlayer().switchSrc(-1);
            //             }
            //         },5000);
            //         _this.autoNeedSet = false;
            //     }
            // });

            this.bind( 'firstPlay', function(){
                _this.firstPlay = true;
            });

			this.bind( 'SourceChange', function(){
				var selectedSrc = _this.getPlayer().mediaElement.selectedSource;
				var selectedId = selectedSrc.getAssetId();

				//if selected source is not part of the menu, show the source before it as the selected one
				//workaround when auto switch with kplayer occurred and the selected source is not part of the menu data provider
				if ( selectedSrc.skip ) {
					var sources = _this.getSources();
					for ( var i = 0; i< sources.length; i++ ) {
						//look for the closest flavor
						if ( selectedSrc.getSrc() == sources[i].getSrc() ) {
							if ( i == 0 && sources.length > 1 ) {
								selectedId = sources[i+1].getAssetId();
							} else {
								selectedId = sources[i-1].getAssetId();
							}
							break;
						}
					}
				}
				_this.getMenu().setActive({'key': 'id', 'val': selectedId});
			});

			this.bind( 'sourceSwitchingStarted', function(){
				_this.getComponent().find('button').addClass( 'in-progress-state' );
				_this.onDisable();
			});
			this.bind( 'sourceSwitchingEnd', function(newIndex){
				_this.getComponent().find('button').removeClass( 'in-progress-state' );
                _this.onEnable();
			});
            this.bind( 'onHideControlBar', function(){
                if ( _this.getMenu().isOpen() )
                    _this.getMenu().close();
            });
			this.bind( 'onChangeMedia', function(){
				_this.sourcesList = [];
			});

			this.bind( 'onDisableInterfaceComponents', function(e, arg ){
				_this.getMenu().close();
			});

			// Check for switch on resize option
			if( this.getConfig( 'switchOnResize' ) && !_this.embedPlayer.isLive() ){
				this.bind( 'resizeEvent', function(){
					// workaround to avoid the amount of 'updateLayout' events
					// !seeking will avoid getting current time equal to 0
					if ( !_this.inUpdateLayout && !_this.embedPlayer.seeking ){
						_this.inUpdateLayout = true;
						_this.updateLayoutTimout = setTimeout(function() {
							_this.inUpdateLayout = false;
						},1000);
						//if we're working with kplayer - mp4 can't be seeked - so disable this feature
						//this only effect native for now
						if (_this.embedPlayer.instanceOf === "Native" && !_this.embedPlayer.isInSequence() ) {
							// TODO add additional logic for "auto" where multiple bitrates
							// exist at the same resolution.
							var selectedSource = _this.embedPlayer.mediaElement.autoSelectSource(_this.embedPlayer.supportsURLTimeEncoding(), _this.embedPlayer.startTime, _this.embedPlayer.pauseTime);
							if ( selectedSource ) { // source was found
								_this.embedPlayer.switchSrc( selectedSource );
							}
						} else {
							mw.log( "sourceSelector - switchOnResize is ignored - Can't switch source since not using native player or during ad playback");
						}
					}
				});
			}

			this.embedPlayer.bindHelper("propertyChangedEvent", function(event, data){
				if ( data.plugin === _this.pluginName ){
					if ( data.property === "sources" ){
						_this.getMenu().$el.find("li a")[data.value].click();
					}
				}
			});
		},
		getSources: function(){
			return this.getPlayer().getSources();
		},

		buildMenu: function(){
			var _this = this;

			// Destroy old menu
			this.getMenu().destroy();
            this.sourcesList = [];
            this.autoAdded = false;
			var sources = this.getSources().slice(0);

            //
            // removing specified flavors size sources
            //
            if (_this.embedPlayer.getFlashvars('removeFlavors') !== undefined) {
                var flavorsToRemove = _this.embedPlayer.getFlashvars('removeFlavors').split(',');

                $.each(sources, function (indexAsset, flavorAsset) {
                    $.each(flavorsToRemove, function (indexRemove, flavorToRemove) {
                        if (flavorAsset && flavorAsset.height == flavorToRemove) {
                            sources.splice(indexAsset, 1);
                            return;
                        }
                    });
                });
            }

			if( ! sources.length ){
				_this.log("Error with getting sources");
				return ;
			}

            //add Auto for addaptive bitrate streams
            if ( !this.handleAdaptiveBitrateAndContinue() )
                return;

            if (this.getConfig('hideSource')) {
                this.getPlayer().mediaElement.removeSourceFlavor(sources);
            }

			if( sources.length == 1 ){
				// no need to do building menu logic.
				this.addSourceToMenu( sources[0], _this.getSourceTitle(sources[0]) );
				return ;
			}

			if(mw.isDesktopSafari()){
				this.addAutoToMenu();
				return;
			}

			// sort by height then bitrate:
			sources.sort(function(a,b){
				var hdiff = b.getHeight() - a.getHeight();
				if( hdiff != 0 ){
					return hdiff;
				}
				return b.getBitrate() - a.getBitrate();
			});

			// if simple format don't include more then two sources per size in menu
			if( _this.getConfig( 'simpleFormat' ) ){
				var prevSource = null;
				var twice = false;
				$.each( sources, function( sourceIndex, source ) {
					if( ! prevSource ){
						prevSource = source;
						return true;
					}
					if( source.getHeight() != 0
						&&
						( _this.getSourceSizeName( prevSource )
							==
							_this.getSourceSizeName( source ) )
						){
						//if the selected source has the same height, skip this source
						var selectedSrc = _this.getPlayer().mediaElement.selectedSource;
						if ( selectedSrc
							&&
							!_this.isSourceSelected( source )
							&&
							!_this.isSourceSelected( prevSource )
							&&
							( _this.getSourceSizeName( source )
								==
								_this.getSourceSizeName( selectedSrc ) )
							){
							source.skip = true;
						}
						else if( twice ){
							// don't skip if this is the default source:
							if( !_this.isSourceSelected( source ) ){
								// skip this source
								source.skip = true;
							} else {
								source.skip = false;
							}
							prevSource = source;
							return true;
						}
						// set the first source as "HQ"
						prevSource.hq = true;
						twice = true;
					} else {
						twice = false;
					}
					// always update prevSource
					prevSource = source;
				});
			}
			var items = [];
			var itemLabels = [];
			var prevSource = null;
			var sourceLabels=[];
			var filterSources=[];

			$.each( sources, function( sourceIndex, source ) {
				var supportingPlayers = mw.EmbedTypes.getMediaPlayers().getMIMETypePlayers( source.getMIMEType() );
				for ( var i = 0; i < supportingPlayers.length ; i++ ) {
					if(
						(
							_this.getPlayer().selectedPlayer === undefined
								&&
								supportingPlayers[i].library == 'Native'
							)
							||
							(
								_this.getPlayer().selectedPlayer !== undefined
									&&
									supportingPlayers[i].library == _this.getPlayer().selectedPlayer.library
								)
					){

						//Not the right time to add to the menu since we need to have proper titles before
						//_this.addSourceToMenu( source );

						var label = _this.getSourceTitle(source);
						var pastIndex=$.inArray(label,sourceLabels);

						//If title equal looking and bandwidth to insert the HQ
						if(pastIndex!=-1 && source.bandwidth!=filterSources[pastIndex].bandwidth){
							if(source.bandwidth>filterSources[pastIndex].bandwidth){
								label+=" HQ";
							}else{
								sourceLabels[pastIndex]+=" HQ";
							}
						}

						if($.inArray(label, sourceLabels) === -1){
							sourceLabels.push(label);
							filterSources.push(source);
						}


						if ($.inArray(label, itemLabels) === -1){
							itemLabels.push(label)
							items.push({'label':label, 'value':label});
						}
						if (_this.embedPlayer.isMobileSkin() && _this.isSourceSelected(source)){
							_this.getMenu().setActive(sourceIndex);
						}
					}
				}
			});

			//After setting the all the titles we add to the menu th br's
			$.each(sourceLabels, function(labelIndex, sourceLabel) {
				_this.addSourceToMenu(filterSources[labelIndex],sourceLabel);
			});

			// dispatch event to be used by a master plugin if defined
			this.getPlayer().triggerHelper("updatePropertyEvent",{"plugin": this.pluginName, "property": "sources", "items": items, "selectedItem": this.getMenu().$el.find('.active a').text()});

		},
		isSourceSelected: function( source ){
			var _this = this;
			return ( _this.getPlayer().mediaElement.selectedSource && source.getSrc()
				==
				_this.getPlayer().mediaElement.selectedSource.getSrc()
				) || source.bandwidth == $.cookie('EmbedPlayer.UserBandwidth');
		},
        handleAdaptiveBitrateAndContinue: function (){
            //Silverlight smoothStream
            if( ( this.getPlayer().streamerType === "smoothStream" ) ){
                this.addAutoToMenu();
                return true;
            }

            //HLS, HDS
            if (mw.isNativeApp()) {
            	this.sourcesList = [];
                this.addAutoToMenu();
                return true;
            }

            if ( this.embedPlayer.streamerType != "http" && this.embedPlayer.streamerType != "hls" && !this.getPlayer().isPlaying() ){
                if(this.embedPlayer.streamerType !== "hls" && !mw.EmbedTypes.getMediaPlayers().isSupportedPlayer('kplayer')){ //If flash disabled, player fallback to http progressive, but the streamerType might still be hdnetwork
                    return true;
                }
                this.addAutoToMenu();
                return false;
            }

            if( this.getPlayer().streamerType != "http" && this.firstPlay ){ //add and select Auto for adaptive bitrate
                this.addAutoToMenu();
                this.firstPlay = false;
            }
            
            if(this.embedPlayer.streamerType === "hls"){
                this.addAutoToMenu();
            }
            return true;
        },
        addAutoToMenu: function (){
            var _this = this;
            if(_this.autoAdded) return;
            _this.autoAdded = true;
            this.getMenu().addItem({
                'label': _this.AutoTitle,
                'callback': function () {
                    if(!mw.isIOS()){
                        _this.getPlayer().switchSrc(-1);
                    }
                },
               'active': $.cookie('EmbedPlayer.HlsLevel') == null || $.cookie('EmbedPlayer.HlsLevel') == -1
            });
        },
		addSourceToMenu: function( source ,sourceLabel){
			var _this = this;

			//We should use the param instead of getting again
            //var sourceLabel = this.getSourceTitle( source );
            if( $.inArray(sourceLabel, this.sourcesList) == -1 ) {

                this.sourcesList.push(sourceLabel);

                this.getMenu().addItem({
                    'label': sourceLabel,
                    'attributes': {
                        'id': source.getAssetId()
                    },
                    'callback': function () {
                    	if(_this.embedPlayer.isLive() && mw.isEdge()) return;
	                    _this.getPlayer().triggerHelper("newSourceSelected", source.getAssetId());
                        _this.getPlayer().switchSrc(source);
                    },
                    'active': _this.isSourceSelected(source)
                });
            }
		},
		//We need to use the closest and not the inferior
		getSourceSizeName: function(source) {
			//Array of available br's
			var bitRateOptions=[144,240,360,480,720,1080];
			var height=source.getHeight();

			function getClosestBr(value, brOptions){
				if(brOptions.length){
					var closest=getClosestBr(brOptions.pop(),brOptions);
					return (Math.abs(height-closest) < (Math.abs(height-value))? closest:value);
				}
				return value;
			}
			return getClosestBr(bitRateOptions.pop(),bitRateOptions)+"P";
		},
		getSourceTitle: function( source ){
			// We should return "Auto" for Apple HLS
			if( source.getMIMEType() == 'application/vnd.apple.mpegurl' ) {
				return this.AutoTitle;
			}

            var title = '';
            switch( this.getConfig( 'displayMode' ) ){
                case 'size' :
                    title = this.getSourceTitleSize(source);
                    break;
                case 'sizefrombitrate' :
                    title = this.getSizeFromBR(source);
                    break;
                case 'bitrate' :
                    title = this.getSourceTitleBitrate(source);
                    break;
                case 'sizebitrate' :
                    title = this.getSourceTitleSizeBitrate(source);
                    break;
            }
            return title;
		},

        /**
         * return size of the video calculated from bitrate not the width
         * @param source
         * @returns {string} title to display in the menu
         * resolutions ranges from
         * https://support.google.com/youtube/answer/2853702?hl=en
         */
        getSizeFromBR: function (source) {
            var br = source.bandwidth;
            if (mw.isIOS() || mw.isAndroid() || this.embedPlayer.streamerType === 'http') br = br / 1000;
            if( br <= 250 ){
                return '144P';
            } else if( br <= 600 ){
                return '240P';
            } else if( br <= 1000 ){
                return '360P';
            } else if( br <= 1500 ){
                return '480P';
            } else if( br <= 3200 ){
                return '720P HD';
            } else {
                return '1080P HD';
            }
        },

        getSourceTitleSize: function( source ){
            var title = '';
            if( source.getHeight() ){
                title = this.getSourceSizeName( source );
                if( this.getConfig( 'displayMode' ) === 'size' && this.getConfig( 'simpleFormat' ) && source.hq ){
                    title += ' HQ';
                }
            } else { //fallback for a case we don't have a frame size (height) for the source (for example HLS source)
                title = this.getSourceTitleBitrate(source);
            }
            return title;
        },
        getSourceTitleBitrate: function( source ){
            var title = '';
            if ( source.getBitrate() ) {
                var bits = ( Math.round( source.getBitrate() / 1024 * 10 ) / 10 ) + '';
                // -------------------------- SUK ---------------------------
                // if bitrate is commint from chromeles kdp multiply it by 1000
                //
                if ((kWidget.isIOS() || kWidget.supportsFlash()) && !this.embedPlayer.plugins.hlsjs) {
                    bits = bits * 1000;
                }
                if(bits == 0) bits = '56';
                if( bits[0] == '0' ){
                    bits = bits.substring(1);
                }
                title+= ' ' + bits + ' kbs ';
            }
            return title;
        },
        getSourceTitleSizeBitrate: function( source ){
            var title = '';
            if ( source.getHeight() ){
                title = this.getSourceSizeName( source ) + ' ';
            }
			title += this.getSourceTitleBitrate(source);
            return title;
        },
		toggleMenu: function(){
			if ( this.isDisabled ) {
				return;
			}
			this.getMenu().toggle();
		},
		getComponent: function() {
			var _this = this;
			if( !this.$el ) {
				var $menu = $( '<ul />' );
				var $button = $( '<button />' )
					.addClass( 'btn icon-cog' )
					.attr('title', _this.selectSourceTitle)
					.click( function(e){
						_this.toggleMenu();
					});
				this.setAccessibility($button,_this.selectSourceTitle);
				this.$el = $( '<div />' )
					.addClass( 'dropup' + this.getCssClass() )
					.append( $button, $menu );
			}
			return this.$el;
		},
		getMenu: function(){
			if( !this.menu ) {
				this.menu = new mw.KMenu(this.getComponent().find('ul'), {
					tabIndex: this.getBtn().attr('tabindex')
				});
			}
			return this.menu;
		},
		getBtn: function(){
			return this.getComponent().find( 'button' );
		},
		onEnable: function(){
			this.isDisabled = false;
			this.updateTooltip( this.selectSourceTitle );
			this.getBtn().removeClass( 'disabled' );
			if (this.saveBackgroundColor){
                this.getComponent().find('button').css('background-color', this.saveBackgroundColor + ' !important');
			}
		},
		onDisable: function(){
			this.isDisabled = true;
			this.updateTooltip( this.switchSourceTitle );
			this.saveBackgroundColor = this.getComponent().find('button').css("background-color");
			this.getComponent().find('button').css('background-color', 'transparent !important');
			this.getComponent().removeClass( 'open' );
			this.getBtn().addClass( 'disabled' );
		}
	}));

} )( window.mw, window.jQuery );
