/**
 * Created by karol.bednarz on 5/11/2016.
 * plugin version 1.1
 */
(function (mw, $, kWidget) {
    "use strict";

    mw.PluginManager.add('comScoreDigitalAnalytix', mw.KBaseComponent.extend({

            defaultConfig: {
                'measurementDomain': null,  // for example "http://uk.sitestat.com/randa/test/s?"
                'publisherName': null,      // publisher name
                'debug': false               // if true activate debug
            },

            eventTrackList: [
                'changeMedia',
                'cleanMedia',
                'entryFailed',
                'mediaLoadError',
                'mediaError',
                'doPause',
                'doPlay',
                'doStop',
                'doSeek',
                'kdpEmpty',
                'playerPaused',
                'playerPlayed',
                'playerSeekStart',
                'playerSeekEnd',
                'playerPlayEnd',
                'playerDimensionChange',
                'openFullScreen',
                'closeFullScreen',
                'changeVolume',
                'volumeChanged',
                'freePreviewEnd',
                'adStart',
                'adEnd',
                'cuePointReached',
                'adOpportunity',
                'playing'
            ],

            streamSense: null,
            firstSeekNotification: true,
            adsList: [],
            currentAd: {adNumber: 0},
            PLUGIN_STATES_ENUMS: {
                UNINIT: -1,
                INIT: 0,
                AD_PLAYING: 1,
                AD_END: 2,
                BUFFERING: 3,
                PLAYING_CONTENT: 4,
                PAUSED: 5,
                PLAY_END: 6
            },
            currentState: -1,
            clipNumberMap: {},
            clipNumberCounter: 0,
            firstTimePlay: true,

            setup: function () {
                //load stream sense sdk
                var _this = this;
                var loadSDK = $.ajax({
                    url: mw.getMwEmbedPath() + "modules/ComScoreDigitalAnalytix/resources/streamsense.4.1505.18.min.js",
                    dataType: "script",
                    timeout: 5000,
                    cache: true
                });

                $.when(loadSDK).then(function () {
                    _this.addPlayerBindings();
                }, function () {
                    // error msg
                });
            },

            /**
             * reseting all properties
             */
            resetDAxValues: function () {
                this.firstSeekNotification = true;
                this.adsList = [];
                this.currentAd = {adNumber: 0};
                this.currentState = -1;
                this.clipNumberMap = {};
                this.clipNumberCounter = 0;
                this.currentState = this.PLUGIN_STATES_ENUMS.UNINIT;
                this.firstTimePlay = true;
            },

            /**
             * initialize the comScore plugin
             */
            settingsInit: function () {
                if (this.currentState == this.PLUGIN_STATES_ENUMS.UNINIT) {
                    this.currentState = this.PLUGIN_STATES_ENUMS.INIT;
                    var measurementDomain = this.getConfig("measurementDomain");
                    this.streamSense = measurementDomain ? new ns_.StreamSense({}, measurementDomain) : new ns_.StreamSense();
                    this.setPersistenceLabel();
                    this.setChangeableLabels();
                    this.streamSense.setPlaylist({ns_st_pl: this.embedPlayer.kalturaPlayerMetaData.name});
                }
            },

            /**
             * setting up persistent label with the information about the player
             */
            setPersistenceLabel: function () {
                this.streamSense.setLabel("ns_st_mp", "Stream AMG Player");
                if (this.getConfig("publisherName"))
                    this.streamSense.setLabel("ns_st_pu", this.getConfig("publisherName"));
                this.streamSense.setLabel("ns_st_mv", window["MWEMBED_VERSION"]);
                this.streamSense.setLabel("ns_st_it", "c");
            },

            setChangeableLabels: function () {
                if (!this.streamSense) return;
                this.embedPlayer.layoutBuilder.fullScreenManager.isInFullScreen() ? this.streamSense.setLabel("ns_st_ws", "full") : this.streamSense.setLabel("ns_st_ws", "norm");
                this.streamSense.setLabel("ns_st_cs", this.embedPlayer.getVideoHolder().width() + 'x' + this.embedPlayer.getVideoHolder().height());
                this.streamSense.setLabel("ns_st_vo", Math.floor(this.embedPlayer.volume * 100));
                this.streamSense.setLabel("ns_st_br", this.getCurrentBandwidth());
            },

            /**
             * return current bandwidth
             * fix for safari can not recognize current playing flavore bitrate
             */
            getCurrentBandwidth: function () {
                var currentBandwidth = Math.floor(this.embedPlayer.currentBitrate);
                return !isNaN(currentBandwidth) ? currentBandwidth : Math.floor(this.embedPlayer.getCurrentBitrate() / 1000);
            },

            /**
             * prepare current clip values
             */
            prepareClip: function (playEnd) {
                var currentClip = {
                    ns_st_cn: this.getClipNumber(this.getClipId()),         // The clip number
                    ns_st_cl: this.getClipDuration(),                       // Length of the stream (milliseconds)
                    ns_st_el: this.getClipDuration(),                       // Length of the stream (milliseconds)
                    ns_st_pn: this.getSegmentNumber(playEnd),                      // This is part (segment) 1 ...
                    ns_st_tp: this.getTotalNumberOfFragments(),             // ... of 1 part in total
                    ns_st_pr: this.embedPlayer.kalturaPlayerMetaData.name,  // Program title
                    ns_st_ep: this.embedPlayer.kalturaPlayerMetaData.name,  // Program title
                    ns_st_ty: this.getContentType(),
                    ns_st_ct: this.getClassificationType(),                 // Classification type
                    c3: "*null",                                            // Dictionary classification value
                    c4: "*null",                                            // Unused dictionary classification value
                    c6: "*null"                                             // Unused dictionary classification value
                };

                if (this.embedPlayer.isInSequence()) {
                    currentClip.ns_st_ad = this.getAdType();
                    currentClip.ns_st_adid = this.currentAd.id; // or we can use add title but sometimes is  this.embedPlayer.kAds.adPlayer.currentAdSlot.ads[0].title
                }
                else {
                    currentClip.ns_st_ci = this.embedPlayer.kentryid;
                }

                // live stream flag
                if (this.embedPlayer.isLive())
                    currentClip.ns_st_li = "1";

                this.streamSense.setClip(currentClip);
                this.streamSense.getClip().setLabels(currentClip)
            },

            /**
             * Type of stream.
             * @returns {string}
             */
            getContentType: function () {
                if (this.embedPlayer.isInSequence())
                    return "ad";
                return this.embedPlayer.isLive() ? "live" : "vod";
            },

            /**
             * return clip duration
             * @returns {number}
             */
            getClipDuration: function () {
                return this.currentState == this.PLUGIN_STATES_ENUMS.AD_PLAYING ? this.currentAd.duration * 1000 : this.embedPlayer.duration * 1000;
            },

            /**
             * return clip id
             * @returns {number}
             */
            getClipId: function () {
                return this.currentState == this.PLUGIN_STATES_ENUMS.AD_PLAYING ? this.currentAd.adNumber : this.embedPlayer.kentryid;
            },

            /**
             * If content is divided in parts (by
             * mid-roll ads), this identifies each of
             * the segments.
             * @returns current segment
             */
            getSegmentNumber: function (playEnd) {
                var _this = this;
                if (this.embedPlayer.isInSequence())
                    return 1;
                if(playEnd){
                    return _this.getTotalNumberOfFragments();
                }
                var segmentNumber = 1;
                if (this.embedPlayer.kCuePoints && this.embedPlayer.kCuePoints.midCuePointsArray) {
                    $.each(this.embedPlayer.kCuePoints.midCuePointsArray, function (index, cuePoint) {
                        if (_this.embedPlayer.currentTime * 1000 <= cuePoint.startTime) {
                            segmentNumber = index + 1;
                            return false;
                        }
                        segmentNumber = index + 2;
                    })
                }

                return segmentNumber;
            },

            /**
             * Episode total number of parts.
             * return number of content fragments
             */
            getTotalNumberOfFragments: function () {
                var numberOfFragments = 1;
                if (this.currentState == this.PLUGIN_STATES_ENUMS.AD_PLAYING)
                    return 1;

                // calculate only number of mid-rolls
                if (this.embedPlayer.playerConfig.plugins.vast && this.embedPlayer.playerConfig.plugins.vast.trackCuePoints == true && this.embedPlayer.kCuePoints && this.embedPlayer.kCuePoints.midCuePointsArray)
                    numberOfFragments += this.embedPlayer.kCuePoints.midCuePointsArray.length;

                // in case we need to calculate numer with pre and post
                //if (this.embedPlayer.playerConfig.plugins.vast) {
                //    numberOfFragments += this.embedPlayer.playerConfig.plugins.vast.preSequence + this.embedPlayer.playerConfig.plugins.vast.postSequence;
                //}
                return numberOfFragments;
            },

            /**
             * return clip number
             * @param mediaId
             * @returns number of the clip
             */
            getClipNumber: function (mediaId) {
                var cn = this.clipNumberMap[mediaId];
                if (cn) {
                    return cn;
                }
                this.clipNumberCounter++;
                this.clipNumberMap[mediaId] = this.clipNumberCounter;

                return this.clipNumberCounter;
            },

            /**
             * The ns_st_ct parameter is critical for enabling comScore to distinguish ad streams from content streams.
             * Please use the table below as a guide for determining the ns_st_ctvalue to place in the tag.
             * @returns {string}
             */
            getClassificationType: function () {
                var _this = this;
                var mediaType = _this.embedPlayer.kalturaPlayerMetaData ? _this.embedPlayer.kalturaPlayerMetaData.mediaType : 1;

                // first letter - stands for content type a-audio v-video
                var ct = mediaType == 5 ? "a" : "v";

                // recognize if advertisement or simple content
                if (this.currentState == this.PLUGIN_STATES_ENUMS.AD_PLAYING) {
                    switch (this.currentAd.type) {
                        case "pre-roll":
                            ct += "a11";
                            break;
                        case "mid-roll":
                            ct += "a12";
                            break;
                        case "post-roll":
                            ct += "a13";
                            break;
                        case "bumper":
                            // TODO: ask comScore if it should be vv99 || va99 || va00?
                            ct += "v99";
                            break;
                        default :
                            ct += "a00";
                    }
                } else {
                    ct += "c";
                    // short form long form or live stream
                    if (this.embedPlayer.isLive()) {
                        ct += "23";
                    } else {
                        // by default set the classification as PREMIUM
                        this.embedPlayer.duration < 600 ? ct += "11" : ct += "12";
                        //TODO: discuss if we want to have USER-GENERATED recognition "21" & "22" (please look at the documentation)
                    }
                }
                return ct;
            },

            /**
             *  Add the player bindings
             */
            addPlayerBindings: function () {
                var _this = this;

                _this.embedPlayer.bindHelper('embedPlayerError', function (e, error) {
                    _this.sendErrorNotification();
                });

                //if spinner is shown inform that there is buffering action
                _this.bind('onAddPlayerSpinner', function () {
                    _this.buffering = true;
                });
                _this.bind('onRemovePlayerSpinner', function () {
                    _this.buffering = false;
                    if (_this.currentState == _this.PLUGIN_STATES_ENUMS.BUFFERING) {
                        _this.streamSense.notify(ns_.StreamSense.PlayerEvents.PLAY, {}, Math.floor(_this.embedPlayer.currentTime * 1000));
                        _this.currentState = _this.embedPlayer.isInSequence() ? _this.PLUGIN_STATES_ENUMS.AD_PLAYING : _this.PLUGIN_STATES_ENUMS.PLAYING_CONTENT;
                    }
                });

                // track bitrate change events
                this.bind('sourceSwitchingEnd', function (newIndex) {
                    _this.setChangeableLabels();
                });

                // set the rest of the bindings from the track list
                $.each(_this.eventTrackList, function () {
                    var eventName = this;
                    var eventNameBinding = _this.getEventNameBinding(eventName);
                    _this.embedPlayer.addJsListener(eventNameBinding + _this.bindPostFix, function (data) {
                        _this.playerEvent(eventName, data);

                    });
                });

                _this.embedPlayer.bindHelper('onAdOpen.dax', function (event, adId, networkName, type, index) {
                    _this.currentAd["id"] = adId;
                    _this.currentAd["type"] = type;
                });

                _this.embedPlayer.bindHelper('AdSupport_StartAdPlayback.dax', function (event, type) {
                    _this.currentAd["type"] = type;
                });

                _this.embedPlayer.bindHelper('AdSupport_AdUpdateDuration.dax', function (event, duration) {
                    if (_this.getConfig("debug") == true)
                        console.warn("__[event] = AdSupport_AdUpdateDuration")
                    _this.currentState = _this.PLUGIN_STATES_ENUMS.AD_PLAYING;
                    _this.currentAd["duration"] = Math.floor(duration);
                    _this.currentAd["adNumber"] = _this.currentAd["adNumber"] + 1;
                    _this.prepareClip();
                    _this.traceCurrentClip("AdSupport_AdUpdateDuration +++adPlay", 0);
                    _this.streamSense.notify(ns_.StreamSense.PlayerEvents.PLAY, {}, 0);
                });

                _this.embedPlayer.bindHelper('AdSupport_AdUpdatePlayhead', function (e, currentTime) {
                    _this.currentAd["currentTime"] = currentTime;
                });
            },

            /**
             * set polling timer to track buffering state
             */
            setPollingTimer: function () {
                var _this = this;
                this.removePollingTimer();
                this.positionPollingTimer = setInterval(function () {
                    if (_this.embedPlayer.buffering && _this.buffering && _this.embedPlayer.currentTime == _this.lastPosition && _this.currentState != _this.PLUGIN_STATES_ENUMS.BUFFERING) {
                        _this.traceCurrentClip("buffering", _this.embedPlayer.currentPosition * 1000);
                        _this.streamSense.notify(ns_.StreamSense.PlayerEvents.BUFFER, {ns_st_ev: "buffer"}, Math.floor(_this.embedPlayer.currentTime * 1000));                        
                        _this.currentState = _this.PLUGIN_STATES_ENUMS.BUFFERING;
                    }
                    _this.lastPosition = _this.embedPlayer.currentTime;
                }, 500);
            },

            /**
             * remove timer for buffering check
             */
            removePollingTimer: function () {
                if (this.positionPollingTimer)
                    clearInterval(this.positionPollingTimer);
                this.positionPollingTimer = null;
            },

            /**
             * return ad type in comScore format
             * @returns {*}
             */
            getAdType: function () {
                var currentAdType = this.currentAd.type;
                if (currentAdType.indexOf("roll") != -1)
                    return currentAdType.substring(0, currentAdType.indexOf("roll")) + "-" + currentAdType.substring(currentAdType.indexOf("roll"), currentAdType.length)
                return currentAdType;
            },


            /**
             * Handles the mapping for special case eventNames that
             * don't match their corresponding kaltura listener binding name
             */
            getEventNameBinding: function (eventName) {
                switch (eventName) {
                    case 'quartiles':
                        return 'playerUpdatePlayhead';
                        break;
                    default :
                        return eventName;
                }
            },

            /**
             * notify errors
             */
            sendErrorNotification: function () {
                var _this = this;
                var error = this.embedPlayer.getError();
                this.streamSense.notify(ns_.StreamSense.PlayerEvents.CUSTOM,
                    {
                        ns_st_ev: "error",
                        error_description: error
                    },
                    Math.floor(_this.embedPlayer.currentTime * 1000)
                );
            },

            /**
             * debug tracing of event and variable
             * @param str
             * @param position
             */
            traceCurrentClip: function (str, position) {
                if (this.getConfig("debug") == true) {
                    console.warn("======== comScore tracking ======");
                    console.log("from " + str);
                    console.log(" ns_st_po=" + position + " ns_st_cl=" + this.streamSense.getClip().getLabel("ns_st_cl") + " ns_st_el=" + this.streamSense.getClip().getLabel("ns_st_el") + " ns_st_pn=" + this.streamSense.getClip().getLabel("ns_st_pn") + " ns_st_tp=" + this.streamSense.getClip().getLabel("ns_st_tp") + " ns_st_ad=" + this.streamSense.getClip().getLabel("ns_st_ad") + " ns_st_ci=" + this.streamSense.getClip().getLabel("ns_st_ci") + " ns_st_pr=" + this.streamSense.getClip().getLabel("ns_st_pr") + " ns_st_ty=" + this.streamSense.getClip().getLabel("ns_st_ty"));
                }
            },

            /**
             * player event handler
             * @param methodName
             * @param data
             */
            playerEvent: function (methodName, data) {
                if (this.getConfig("debug") == true)
                    console.warn("[event] = ", methodName, data)
                var _this = this;
                switch (methodName) {
                    case "adStart":
                        // play notification is handled when duration is recieved in AdSupport_AdUpdateDuration listener
                        if (data.timeSlot != "preroll") {
                            this.traceCurrentClip("adStart +++adStart --send clipEnd", (this.currentAd.duration * 1000));
                            this.streamSense.notify(ns_.StreamSense.PlayerEvents.END, {}, Math.floor(this.embedPlayer.currentTime * 1000));
                        }
                        break;
                    case "adEnd":
                        this.traceCurrentClip("adEnd +++adEnd", (Math.ceil(_this.currentAd["currentTime"]) * 1000));
                        var labelsObject = {};
                        if (this.currentAd.type == "postroll") {
                            labelsObject = {ns_st_pe: 1};
                            this.currentState != this.PLUGIN_STATES_ENUMS.PLAY_END
                        } else {
                            this.currentState = this.PLUGIN_STATES_ENUMS.AD_END;
                        }
                        var currentAdTime = Math.min(Math.ceil(_this.currentAd["currentTime"]), _this.currentAd.duration)
                        this.streamSense.notify(ns_.StreamSense.PlayerEvents.END, labelsObject, ( currentAdTime * 1000));
                        break;
                    case "changeMedia":
                        _this.resetDAxValues();
                        _this.settingsInit();
                        break;
                    case "doPlay":
                        _this.settingsInit();
                        // calculate time from the moment when pressed play
                        if (!this.embedPlayer.isInSequence() && this.firstTimePlay) {
                            this.doPlayTriggeredTime = new Date();
                        }
                        break;
                    case "playerPlayed":
                        this.currentState = this.embedPlayer.isInSequence() ? this.PLUGIN_STATES_ENUMS.AD_PLAYING : this.PLUGIN_STATES_ENUMS.PLAYING_CONTENT;
                        // handle only content playing event, adPlaying can be handle when duration of the add is updated in AdSupport_AdUpdateDuration listener
                        if (!this.embedPlayer.isInSequence()) {
                            var currentTime = Math.floor(this.embedPlayer.currentTime * 1000);
                            var customObj = {};
                            if (this.firstTimePlay) {
                                this.firstTimePlay = false;
                                var delay = new Date() - _this.doPlayTriggeredTime;
                                customObj = {ra_pbd: delay};
                                currentTime = 0;
                            }

                            this.setPollingTimer();
                            this.prepareClip();
                            this.traceCurrentClip("playing +++clipPlay", currentTime);
                            this.streamSense.notify(ns_.StreamSense.PlayerEvents.PLAY, customObj, currentTime);
                        }
                        break;
                    case "doStop":
                    case "playerPlayEnd":
                        if (this.currentState != this.PLUGIN_STATES_ENUMS.PLAY_END) {
                            _this.prepareClip(true);
                            this.traceCurrentClip(methodName, (this.embedPlayer.currentTime * 1000));
                            _this.streamSense.setLabel("ns_st_pe", "1");
                            _this.streamSense.notify(ns_.StreamSense.PlayerEvents.END, {ns_st_pe: 1}, Math.floor(_this.embedPlayer.duration * 1000));
                            this.currentState = this.PLUGIN_STATES_ENUMS.PLAY_END;
                            this.resetDAxValues();
                            this.removePollingTimer();
                            this.settingsInit();
                        }
                        break;
                    case "doPause":
                        if (this.currentState != this.PLUGIN_STATES_ENUMS.PLAY_END) {
                            this.traceCurrentClip("doPause", (this.embedPlayer.currentTime * 1000));
                            this.streamSense.notify(ns_.StreamSense.PlayerEvents.PAUSE, {}, Math.floor(this.embedPlayer.currentTime * 1000));
                        }
                        break;
                    case "mediaLoadError":
                    case "mediaError":
                    case "entryFailed":
                    case "kdpEmpty":
                        this.sendErrorNotification();
                        break;
                    case 'playerSeekEnd':
                        if (_this.embedPlayer.isPlaying()) {
                            this.streamSense.notify(
                                ns_.StreamSense.PlayerEvents.PLAY,
                                {ns_st_ui: "seek"},
                                Math.round(_this.embedPlayer.currentSeekTargetTime) * 1000
                            );
                            _this.firstSeekNotification = true;
                        }
                        break;
                    case 'doSeek':
                        // in new version of player while seeking player send 2 doSeek notification
                        // first is actual doSeek caused by mouseClick
                        // second one is made by mw.EmbedPlayer.syncCurrentTime monitor
                        // we are interested with the mouse click only since it contains the current time of scrubber before playhead moved
                        // doSeek coming from mw.EmbedPlayer.syncCurrentTime has the new scrubber position time
                        if (_this.firstSeekNotification && _this.embedPlayer.isPlaying()) {
                            this.streamSense.notify(
                                ns_.StreamSense.PlayerEvents.PAUSE,
                                {ns_st_ui: "seek"},
                                Math.floor(data * 1000)
                            );
                            _this.firstSeekNotification = false;
                        }
                        break;
                    case 'openFullScreen':
                    case 'closeFullScreen':
                    case 'changeVolume':
                        this.setChangeableLabels();
                        break;
                    default :
                        return;
                }
            }
        })
    )
    ;
})
(window.mw, window.jQuery, kWidget);
