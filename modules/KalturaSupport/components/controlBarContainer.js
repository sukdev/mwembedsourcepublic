( function( mw, $ ) {"use strict";

	mw.PluginManager.add( 'controlBarContainer', mw.KBasePlugin.extend({

		defaultConfig: {
			'hover': false
		},

		keepOnScreen: false,

		setup: function(){
			if (this.embedPlayer.isMobileSkin()){
				this.setConfig("hover", true);
			}
			// Exit if we're using native controls
			if( this.getPlayer().useNativePlayerControls() ) {
				this.getPlayer().enableNativeControls();
				return;
			}
			// Set overlay controls to configuration
			this.getPlayer().overlaycontrols = this.getConfig('hover');

			// Bind player
			this.addBindings();
		},
		addBindings: function(){
			var _this = this;
			// Register our container
			this.bind( 'addLayoutContainer', function() {
				_this.getPlayer().getInterface().append( _this.getComponent() );
			});
			this.bind( 'showInlineDownloadLink', function(){
				_this.hide();
			});
			this.bind( 'ended', function(){
				_this.show();
			});
			this.bind( 'layoutBuildDone', function(){
				if (!_this.embedPlayer.isMobileSkin()){
					_this.show();
				}
			});

			// Bind hover events
			if( this.getConfig('hover') ){
				// Show / Hide controlbar on hover
				this.bind( 'showPlayerControls', function(e, data){
					_this.show();

				});
				this.bind( 'hidePlayerControls', function(){
					_this.hide();
				});
				this.bind( 'onComponentsHoverDisabled', function(){
					_this.keepOnScreen = true;
                                        if(!mw.isAndroid())
                                            _this.show();
				});
				this.bind( 'hideScreen closeMenuOverlay', function(){
					if (!_this.embedPlayer.paused){
						_this.keepOnScreen = false;
						_this.hide();
					}else{
						_this.show();
					}
				});
				this.bind( 'onComponentsHoverEnabled showScreen displayMenuOverlay', function(){
					_this.keepOnScreen = false;
					_this.hide();
				});
				this.bind( 'onHideSideBar', function(){
					_this.forceOnScreen = false;
				});
				this.bind( 'onShowSideBar', function(){
					_this.forceOnScreen = true;
				});
                                this.bind('onOpenFullScreen', function() {
                                    if(mw.isAndroid())
                                        _this.forceOnScreen = false;
                                });
			} else {
				this.getPlayer().isControlsVisible = true;
			}
		},
		show: function(){
			if(this.embedPlayer.isMobileSkin() && this.getPlayer().getPlayerPoster().length){
				return; // prevent showing controls on top of the poster when the video first loads
			}
			this.getPlayer().isControlsVisible = true;
			this.getComponent().addClass( 'open' );
			// Trigger the screen overlay with layout info:
			this.getPlayer().triggerHelper( 'onShowControlBar', {
				'bottom' : this.getComponent().height() + 15
			} );
			var $interface = this.embedPlayer.getInterface();
			$interface.removeClass( 'player-out' );
            if(mw.isAndroid()){
                clearTimeout(this.hideTimeout);
                this.hideTimeout = null;
            }

        },
		hide: function(){
            // introducing delay for the android devices hiding hovering control bar
            // AT: #T20160329.0017
            if (mw.isAndroid()) {
                var _this = this;
                if (!this.hideTimeout){
                    this.hideTimeout = setTimeout(function () {
                        _this.callHide();
                    }, 5000);
                }
            }
            else {
                this.callHide();
            }
        },
        callHide: function(){
            if( this.keepOnScreen || this.forceOnScreen) return;
            this.getPlayer().isControlsVisible = false;
            this.getComponent().removeClass( 'open' );
            var $interface = this.embedPlayer.getInterface();
            $interface.addClass( 'player-out' );
            // Allow interface items to update:
            this.getPlayer().triggerHelper('onHideControlBar', {'bottom' : 15} );
        },

    getComponent: function(){
			if( !this.$el ) {
				var _this = this;
				var $controlsContainer = $('<div />').addClass('controlsContainer');
				// Add control bar 				
				this.$el = $('<div />')
								.addClass('controlBarContainer')
								.append( $controlsContainer );

				// Add control bar special classes
				if( this.getConfig('hover') && this.getPlayer().isOverlayControls() ) {
					this.$el.addClass('hover')
						.on("mouseenter", function(){
							_this.forceOnScreen = true;
						})
						.on("mouseleave", function(){ // SUK: click listener has been removed
							_this.forceOnScreen = false;
						});
					this.embedPlayer.getVideoHolder().addClass('hover');
				} else {
					this.$el.addClass('block');
				}
			}
			return this.$el;
		}
	}));

} )( window.mw, window.jQuery );