( function( mw, $ ) {"use strict";
	mw.PluginManager.add( 'liveStatus', mw.KBaseComponent.extend({
		onAirStatus: false,

		defaultConfig: {
			'parent': 'controlsContainer',
			'order': 22,
			'displayImportance': 'high',
			'showTooltip': true,
            'hidden': false // if true don't show the status
		},

		offlineIconClass: 'live-icon offline-icon not-clickable',
		onAirIconClass: 'live-icon online-icon not-clickable',
		unsyncIConClass: 'live-icon live-off-sync-icon',
		noThumbClass: 'not-clickable',

        stringsReady: false,
		liveText: '',
		offlineText: '',
		tooltip: '',

		prevIconClass: undefined,
		bindPostfix: '.LiveStatus',

		setup: function() {
			this.prevIconClass = this.onAirIconClass;
            this.addBindings();
            var _this = this;
            this.bind( 'playerReady', function() {
                if(!_this.stringsReady){
                    _this.initStrings();
                    _this.setLiveStreamStatus();
                }
                if( _this.getPlayer().isLive() ) {
                    _this.addBindings();
                }
            });
			this.bind( 'onChangeMedia', function() {
                _this.removeBindings();
            });
            if(this.getConfig( 'hidden' ) == true)
                this.getComponent()[0].style.visibility = "hidden";
		},
        initStrings: function(){
            this.liveText = gM( 'mwe-embedplayer-player-on-air' );
            this.offlineText = gM( 'mwe-embedplayer-player-off-air' );
            this.tooltip = gM( 'mwe-embedplayer-player-jump-to-live' );
            this.stringsReady = true;
        },
		addBindings: function() {
			var _this = this;
			this.bind( 'liveStreamStatusUpdate' + _this.bindPostfix, function( e, onAirObj ) {
				if ( onAirObj.onAirStatus != _this.onAirStatus ) {
					_this.onAirStatus = onAirObj.onAirStatus;
					_this.setLiveStreamStatus();
				}
			} );
			this.bind( 'movingBackToLive' + _this.bindPostfix, function() {
				if ( _this.onAirStatus ) {
					_this.setLiveUI();
					_this.prevIconClass = _this.onAirIconClass ;
				}
			} );
			this.once('playing' + _this.bindPostfix,function() {
				_this.bind( 'seeked' + _this.bindPostfix + ' seeking' + _this.bindPostfix + ' onpause' + _this.bindPostfix + ' onLiveOffSynchChanged' + _this.bindPostfix , function ( e , param ) {
					if (!_this.getPlayer().goingBackToLive) {
						if ( e.type === 'onLiveOffSynchChanged' && param === false ) {
							// synch with Live edge
							_this.backToLive();
						} else {
							// live is off-synch
							_this.getPlayer().setLiveOffSynch( true );
							if ( _this.onAirStatus ) {
								_this.setOffSyncUI();
							}
							_this.prevIconClass = _this.unsyncIConClass;
						}
					}
				} );
			});
            this.bind( 'onplay' + _this.bindPostfix, function() {
                if ( !_this.getPlayer().isDVR() && !_this.embedPlayer.changeMediaStarted) {
                    // synch with Live edge
                    _this.getPlayer().setLiveOffSynch(false);
                }
            });
		},

        removeBindings: function(){
            this.unbind(  this.bindPostfix );
        },

		getComponent: function() {
			var _this = this;
			if( !this.$el ) {
				var $btnText = $( '<div />')
					.addClass( 'back-to-live-text timers ' + this.noThumbClass + this.getCssClass() )
					.text( this.offlineText );


                var $icon  =$( '<div />' )
                    .addClass( 'btn timers '+ this.offlineIconClass + this.getCssClass() );

                this.$el = $( '<div />')
                .addClass( 'back-to-live' + this.getCssClass() )
                .append( $icon, $btnText ).click( function() {
                    if ( _this.onAirStatus && _this.getPlayer().isDVR() && _this.prevIconClass != _this.onAirIconClass ) {
                            _this.getPlayer().setLiveOffSynch(false);
                        }else{
                            _this.getPlayer().setLiveOffSynch(true);
                        }
                    });
			}
			return this.$el;
		},

		backToLive: function() {
			if ( this.getPlayer().firstPlay )  {
				this.getPlayer().play();
			}  else {
				this.getPlayer().removePoster();
				this.getPlayer().backToLive();
			}
		},

		setOffSyncUI: function() {

        	var isSync = false;
            var hlsjs = this.embedPlayer.plugins.hlsjs;
            // SUK - when using hlsjs plugin relay on actual live time position calculated from streamController
        	if(mw.getConfig('useHlsJS') === true && hlsjs && hlsjs.hls && hlsjs.hls.streamController && hlsjs.hls.streamController.level){
                var level = this.embedPlayer.plugins.hlsjs.hls.streamController.level;
                var levelDetails = this.embedPlayer.plugins.hlsjs.hls.streamController.levels[level].details;
                var start = levelDetails.fragments[0].start;
                var liveSyncPosition = this.embedPlayer.plugins.hlsjs.hls.streamController.computeLivePosition(start, levelDetails);
                isSync = liveSyncPosition - 1 < this.embedPlayer.plugins.hlsjs.hls.media.currentTime;
            }

            if(!isSync){
                this.getComponent('<div/>').removeClass('online').addClass('offline');
                this.getComponent().find('.live-icon').removeClass( this.offlineIconClass + " " + this.onAirIconClass ).addClass( this.unsyncIConClass );
                this.getComponent().find('.back-to-live-text').text( this.liveText );
                this.updateTooltip( this.tooltip );
            }
		},

		setLiveUI: function() {
            this.getComponent('<div/>').removeClass('offline').addClass('online');
			this.getComponent().find('.live-icon').removeClass( this.offlineIconClass + " " + this.unsyncIConClass ).addClass( this.onAirIconClass );
			this.getComponent().find('.back-to-live-text').text( this.liveText );
			this.updateTooltip( "" );
		},

		setLiveStreamStatus: function() {
			if ( this.onAirStatus ) {
				if ( this.prevIconClass == this.unsyncIConClass ) {
					this.setOffSyncUI();
				} else {
					this.setLiveUI();
				}
			}
			else {
				this.getComponent().find('.live-icon').removeClass( this.onAirIconClass + " " + this.unsyncIConClass ).addClass( this.offlineIconClass );
				this.getComponent().find('.back-to-live-text').text( this.offlineText );
				this.updateTooltip( "" );
			}
		}
	}))
} )( window.mw, window.jQuery );

