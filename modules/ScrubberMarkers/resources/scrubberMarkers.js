/**
 * Created by karol.bednarz on 15/06/2016.
 * plugin version 1.4
 */
(function (mw, $, kWidget) {
    "use strict";
    mw.PluginManager.add('scrubberMarkers', mw.KBaseComponent.extend({
            defaultConfig: {
                'order': 1,
                'parent': 'controlsContainer',
                'url': undefined,
                'heartbeatInterval': 60
            },
            duration: 0,
            componentUpdated: false,
            markersAdded: false,
            fred: undefined,
            setup: function () {

                if(!this.embedPlayer.playerConfig.vars.recorded && this.embedPlayer.isLive() && !this.embedPlayer.plugins.hlsjs && ((/Edge/i.test(navigator.userAgent)) || (/Android/i.test(navigator.userAgent))))
                return;

                var _this = this;

                var heartBeat = this.getConfig("heartbeatInterval") * 1000;
                setInterval(function () {
                    _this.getMarkersData();
                }, heartBeat);
                this.bind('updateComponentsVisibilityDone', function (event, duration) {
                    _this.componentUpdated = true;
                    _this.getMarkersData();
                });
                this.bind('durationChange', function (event, duration) {
                    _this.duration = duration;
                    if (_this.duration > 0 && !_this.durationSet) {
                        _this.durationSet = true;
                        _this.componentUpdated = true;
                        _this.updateOverlay()
                    }
                });
                this.embedPlayer.addJsListener('playerSeekEnd', function (event, data) {
                    _this.embedPlayer.markerSeek = 0;
                    //_this.embedPlayer.updatePlayHead(_this.embedPlayer.currentSeekTargetTime / _this.embedPlayer.duration);
                });

                // sample inject on media ready on html site:
                //
                //window.kdp.kBind( 'mediaReady', function(){
                //    window.kdp.sendNotification("injectMarkers",[{"TimeInSeconds":360,"Label":"Pre Show","Icon":1},{"TimeInSeconds":1100,"Label":"First Half","Icon":2},{"TimeInSeconds":2900,"Label":"Second Half","Icon":3},{"TimeInSeconds":3600,"Label":"Full Time","Icon":4}]);
                //})

                this.embedPlayer.addJsListener("injectMarkers", function (json) {
                    _this.markersAdded = false;
                    _this.componentUpdated = true;

                    // remove markers if exist
                    $.each($('[class^="marker"]'), function (key, value) {
                        value.parentNode.removeChild(value);
                    });


                    _this.fred = json;
                    _this.updateOverlay();
                });
            },

            /**
             * retrieve markers data from service
             */
            getMarkersData: function () {

                // remove markers if exist
                $.each($('[class^="marker"]'), function (key, value) {
                    value.parentNode.removeChild(value);
                });

                this.markersAdded = false;
                this.componentUpdated = true;

                var _this = this;
                var url = this.getConfig("url");
                if (url == undefined)
                    return;

                var jsonpCallback = url.slice(url.indexOf("callback") + 9, url.length);
                $.ajax({
                    type: 'GET',
                    url: url,
                    async: false,
                    jsonpCallback: jsonpCallback,
                    contentType: "application/json",
                    dataType: 'jsonp',
                    success: function (json) {
                        _this.fred = json;
                        _this.updateOverlay();
                    },
                    error: function (e) {
                        //if (console != undefined && console.log)
                        //    console.log("markers" + e.message);
                        // markers error
                    }
                });
            },


            /**
             * SUK function calculate the points on scrubber for markers and set the markers titles
             */
            updateOverlay: function () {
                if (!this.markersAdded && this.fred != undefined && this.componentUpdated && this.durationSet == true) {
                    this.markersAdded = true;
                    var labelPositionBottom = 20;
                    var marginLeft = 50;
                    if (this.embedPlayer.plugins.scrubber) {
                        labelPositionBottom = this.embedPlayer.plugins.scrubber.isSliderPreviewEnabled() ? 73 : 20;
                        marginLeft = this.embedPlayer.plugins.scrubber.isSliderPreviewEnabled() ? 7 : 50;
                    }


                    for (var i = 0; i < this.fred.length; i++) {
                        var percentage = parseInt(this.fred[i].TimeInSeconds) / this.duration * 1000;
                        percentage = parseInt(percentage) / 10;
                        this.getComponent().find('.marker' + i + 'time').css({
                            "left": percentage + "%",
                            "z-index": "99",
                            "position": "absolute",
                            "top": "-2px"
                        });
                        var textLength = 104;

                        var appendStyle = '<style>.marker' + i + 'time{ ' +
                            'background: url("' + this.getIcon(i) + '")  no-repeat 0% 50%; ' +
                            'background-size: 100% 100%; ' +
                            'display: block; ' +
                            'width: 18px !important; ' +
                            'height: 18px !important; ' +
                            'margin-top: -2px; margin-left: -7px !important; }' +

                            '.marker' + i + 'tooltip {' +
                            'bottom: ' + labelPositionBottom + 'px;' +
                            'width: ' + textLength + 'px;' +
                            'height: 12px;' +
                            'left: ' + percentage + '%; ' +
                            'margin-left: -' + marginLeft + 'px; ' +
                            'text-align: center; ' +
                            'position: absolute; ' +
                            'background-color: #000000; ' +
                            'background-repeat: repeat-x; ' +
                            'background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#000000), to(#000000)); ' +
                            'background: -webkit-linear-gradient(top, #000000, #000000); ' +
                            'background: -moz-linear-gradient(top, #000000, #000000); ' +
                            'background: -o-linear-gradient(top, #000000, #000000); ' +
                            'background: -ms-linear-gradient(top, #000000, #000000); ' +
                            'background: linear-gradient(top, #000000, #000000); ' +
                            'z-index: 1000; ' +
                            'font-weight: normal; ' +
                            'font-size: 0.9em; ' +
                            'letter-spacing: 0px; ' +
                            'color: #ffffff; ' +
                            'border-radius: 2px; ' +
                            'display: none ' +
                            'padding: 2px 5px; } ' +
                            '</style>';
                        $('head').append(appendStyle);
                    }

                    appendStyle = '<style>.marker-wrapper { position: relative; margin: 0 0.44em;} .controlsContainer {height: 3.5em !important;border-top: 0px solid rgba(170,168,168,0.5);box-shadow: 0px 0px 0px rgba(0,0,0,0);}.currentTimeLabel, .durationLabel{font-size: inherit !important;}.timers{top:-1px;}</style>'
                    $('head').append(appendStyle);
                }

            },

            getText: function (labelNumber) {
                return this.fred[labelNumber].Label.toUpperCase();
            },

            getIcon: function (iconNumber) {
                var iconType = this.fred[iconNumber].Icon;
                var image = mw.getConfig('Kaltura.ServiceUrl') + '/html5/html5lib/v' + window.MWEMBED_VERSION + '/modules/ScrubberMarkers/icons/' + iconType + '.png';
                return image;
            },


            /**
             * get the component, if doesn't existing create one
             * @returns div element
             */
            getComponent: function () {
                var _this = this;
                this.$el = $('.scrubber');

                var markerWrapper = $("<div class='marker-wrapper'></div>");

                this.$el.append(markerWrapper);

                if (this.componentUpdated == true && this.fred) {
                    this.componentUpdated = false;
                    for (var i = 0; i < this.fred.length; i++) {
                        markerWrapper.append(
                            $('<div />')
                                .addClass('marker' + i + 'time').attr('marker-time', _this.fred[i].TimeInSeconds)
                                .attr('elementId', i)
                                .mousemove(function (e) {
                                    var i = parseInt(e.currentTarget.getAttribute("elementId"));
                                    var currentToolTip = $('.marker' + i + 'tooltip');
                                    if ($('.sliderPreview').length > 0 && $('.sliderPreview')[0].style.display != 'none') {
                                        var position = Number($('.sliderPreview')[0].style.left.replace("px", ""));
                                        position += 2;
                                        currentToolTip[0].style["left"] = position + "px";
                                        currentToolTip[0].style["display"] = "block ";

                                    }
                                    currentToolTip.show();
                                })
                                .mouseout(function (e) {
                                    var i = parseInt(e.currentTarget.getAttribute("elementId"));
                                    var currentToolTip = $('.marker' + i + 'tooltip');
                                    currentToolTip[0].style["display"] = "none ";
                                    currentToolTip.hide();
                                })
                                .mousedown(function (e) {
                                    var dpa = parseInt(e.currentTarget.getAttribute("elementId"));
                                    _this.embedPlayer.markerSeek = _this.fred[dpa].TimeInSeconds;
                                    //_this.embedPlayer.updatePlayHead(_this.fred[dpa].TimeInSeconds / _this.embedPlayer.duration);
                                })
                        );
                        var mtt = $('.marker' + i + 'tooltip');
                        if (mtt.length == 0)
                            markerWrapper.append(
                                $('<div >' + _this.getText(i) + '</div>')
                                    .addClass('marker' + i + 'tooltip')
                                    .attr('elementId', i)
                                    .attr('style', 'display:none')
                            );
                    }
                }
                return this.$el;
            }
        })
    );
})
(window.mw, window.jQuery, kWidget);
