(function (mw, $, kWidget) {
	"use strict";

	mw.PluginManager.add('scrubberAMG', mw.KBaseComponent.extend({

		defaultConfig: {
			'parent': 'controlBarContainer',
			'insertMode': 'firstChild',
			'order': 25,
			'sliderPreview': true,
			'thumbSlices': 100,
			'thumbWidth': 100,
			'minWidth': 100,
			'displayImportance': "medium",
			'disableUntilFirstPlay': false,
			'showOnlyTime': false
		},

		waitForFirstPlay: false,
		updateEnabled: true,
        liveEdge: 98,
        load: false,
        seekState: false,

		setup: function (embedPlayer) {

			// make sure insert mode reflects parent type:
			if (this.getConfig('parent') == 'controlsContainer') {
				this.setConfig('insertMode', 'lastChild');
			}
			this.addBindings();
		},
		addBindings: function () {
			var _this = this;
			this.bind('durationWithoutOmitSegments', function (event, duration) {
				_this.duration = duration;
			});
            this.bind('clippingSegments', function(event,segments){
               _this.segments = segments;
            });
            this.embedPlayer.addJsListener('playerSeekStart', function (event,variable1, variable) {
                _this.seekState = true;
            });
            this.embedPlayer.addJsListener('doSeek', function (event,variable1, variable) {
                _this.seekState = true;
            });
            this.embedPlayer.addJsListener('playerSeekEnd', function () {
                _this.seekState = false;
            });
			// check if parent is controlsContainer
			if (this.getConfig('parent') == 'controlsContainer') {
				// need to add
				this.bind('updateComponentsVisibilityStart', function () {
					// take minWidth, so that normal display Importance rules work:
					_this.getComponent().css('width', _this.getConfig('minWidth'));
				});
				this.bind('updateComponentsVisibilityDone', function () {
					var $container = _this.getComponent().parent();
					// get remaining space:
					var compSize = _this.embedPlayer.layoutBuilder.getComponentsWidthForContainer(
						$container
					) - _this.getComponent().width();
					var targetSize = $container.width() - compSize;
					if (targetSize < _this.getConfig('minWidth')) {
						targetSize = _this.getConfig('minWidth');
					}
					_this.getComponent().css('width', ( targetSize ) + 'px');
				});
			}
			// Update buffer bar
			this.bind('updateBufferPercent', function (e, bufferedPercent) {
				_this.updateBufferUI(bufferedPercent);
			});

			this.bindUpdatePlayheadPercent();
			this.bind('externalUpdatePlayHeadPercent', function (e, perc) {
				_this.updatePlayheadPercentUI(perc);
			});
			//will stop listening to updatePlayheadPercent events
			this.bind('detachTimeUpdate', function () {
				_this.unbind('updatePlayHeadPercent');
			});
			//will re-listen to updatePlayheadPercent events
			this.bind('reattachTimeUpdate', function () {
				_this.bindUpdatePlayheadPercent();
			});

			this.bind('onOpenFullScreen', function () {
				// check if IE11 and iframe (KMS-4606)
                if( mw.isIE11() && ( mw.getConfig('EmbedPlayer.IsIframeServer' ) || mw.getConfig('EmbedPlayer.IsFriendlyIframe') ) ) {
                    window["resizeScrubberIE11"] = true; // global var for jquery.ui.slider.js - fix jquery defect inside IE11 iframe fullscreen element.outerWidth()
                }
			});
			this.bind('onCloseFullScreen', function () {
				if( window["resizeScrubberIE11"]===true )
                    window["resizeScrubberIE11"] = null; //clear global var used only by jquery in IE11 iframe fullscreen
			});

			this.bind('playerReady', function (event) {

				if (_this.getConfig('disableUntilFirstPlay')) {
					_this.waitForFirstPlay = true;
					_this.onDisable();
				}
			});

			// If we need to disable the plugin until first play, bind to first play
			if (this.getConfig('disableUntilFirstPlay')) {
				this.waitForFirstPlay = true;
				this.bind('firstPlay', function () {
					_this.waitForFirstPlay = false;
					_this.onEnable();
				});
			}
			this.bind("freezeTimeIndicators", function (e, state) {
				if (state === true) {
					_this.updateEnabled = false;
				} else {
					_this.updateEnabled = true;
				}
			});
            this.bind("onPlayerStateChange", function (e, newState, oldState) {
                if(newState === 'load') {
                    _this.load = true;
                }else
                {
                    _this.load = false;
                }
                if(newState === 'pause') {
                    _this.paused = true;
                }
            });
		},
		bindUpdatePlayheadPercent: function () {
			var _this = this;
			this.bind('updatePlayHeadPercent2', function (e, perc) {
                if(!_this.embedPlayer.userSlide)
                    _this.updatePlayheadPercentUI(perc);
			});
		},
		updatePlayheadPercentUI: function (perc) {
			if (this.updateEnabled && this.seekState == false) {
				var val = parseInt(perc * 1000);
				this.updatePlayheadUI(val);
			}
		},
		updateBufferUI: function (percent) {
            this.getComponent().find('.buffered').css({
				"width": ( parseInt(percent * 100) ) + '%'
			});
		},
		updatePlayheadUI: function (val) {
            if(this.seekState == false) {

                if (this.getPlayer().instanceOf !== 'Native' && this.getPlayer().isPlaying() && !this.paused && this.embedPlayer.isDVR() && !this.load) {
                    this.checkForLiveEdge();
                    if (!this.getPlayer().isLiveOffSynch()) {
                        this.getComponent().slider('option', 'value', 999);
                        return;
                    }
                }
                this.getComponent().slider('option', 'value', val);
                if (this.paused && this.getPlayer().isPlaying()) {
                    this.paused = false;
                }
            }
		},
        checkForLiveEdge: function (){
            var playHeadPercent = (this.getPlayHeadComponent().position().left + this.getPlayHeadComponent().width()/2) / this.getComponent().width();
            playHeadPercent = parseInt(playHeadPercent*100);

            if( this.getPlayer().isLiveOffSynch() && playHeadPercent > this.liveEdge -1 ){
                this.getPlayer().setLiveOffSynch(false);
            }
        },
		onEnable: function () {
			if (this.waitForFirstPlay) return;
			this.isDisabled = false;
			this.getComponent().removeClass('disabled');
			this.getComponent().slider("option", "disabled", false);
		},
		onDisable: function () {
			if (this.isDisabled) return; // do not disable twice
			this.isDisabled = true;
			this.getComponent().slider("option", "disabled", true);
			this.getComponent().addClass('disabled');
		},
		getSliderConfig: function () {
			var _this = this;
			var embedPlayer = this.getPlayer();
            window.embedPlayerson = embedPlayer;
			var alreadyChanged = false;
			return {
				range: "min",
				value: 0,
				min: 0,
				max: 1000,
				// we want less than monitor rate for smoth animation
				animate: mw.getConfig('EmbedPlayer.MonitorRate') - ( mw.getConfig('EmbedPlayer.MonitorRate') / 70 ),
				start: function (event, ui) {
					embedPlayer.userSlide = true;
					// Release the mouse when player is not focused
					$(_this.getPlayer()).one('hidePlayerControls onFocusOutOfIframe', function () {
						$(document).trigger('mouseup');
					});
				},
				slide: function (event, ui) {
					_this.updateAttr(ui);
				},
				change: function (event, ui) {
                    if(_this.seekState == false){
                        alreadyChanged = true;
                        var seekTime = (ui.value / 1000) * _this.duration;
                        seekTime = Math.floor(_this.mapSeekTimeToSegment(seekTime));

                        // always update the title
                        _this.updateAttr(ui);

                        // Only run the onChange event if done by a user slide
                        // (otherwise it runs times it should not)
                        if (embedPlayer.userSlide) {

                            embedPlayer.userSlide = false;
                            _this.seekState = true;
                            embedPlayer.seeking = true;
                            seekTime = _this.increaseIfEdge(seekTime)
                            embedPlayer.seek(seekTime);
                        }
                    }
				}
			};
		},
        increaseIfEdge: function(seekTime){
            for (var i = 0 ; i < this.segments.length ; i++){
                if(seekTime == this.segments[i][0])
                    return seekTime+1;
            }
            return seekTime;
        },

        mapSeekTimeToSegment: function(aSeekTime){
            var totalSegmentsTime = 0;
            for (var i=0 ; i<this.segments.length;i++){
                totalSegmentsTime+= this.segments[i][1] - this.segments[i][0];
                if(aSeekTime <= totalSegmentsTime){
                    var diff = totalSegmentsTime - aSeekTime;
                    return this.segments[i][1] - diff;
                }
            }
        },

		updateAttr: function (ui) {

            var _this = this;
			var perc = ui.value / 1000;

            var $slider = this.$el.find('.ui-slider-handle');
			var title = mw.seconds2npt(perc * _this.duration);

			var attributes = {
				'data-title': title,
				'aria-valuetext': mw.seconds2npt(perc * _this.duration),
				'aria-valuenow': parseInt(perc * 100) + '%'
			};
			$slider.attr(attributes);
			if (this.getConfig('accessibilityLabels')) {
				$slider.html('<span class="accessibilityLabel">' + title + '</span>');
			}
		},
        getPlayHeadComponent: function () {
            return this.getComponent().find('.playHead');
        },
		getComponent: function () {
			var _this = this;
			if (!this.$el) {
				this.$el = $('<div />')
					.attr({
						'role': 'slider'
					})
					.addClass(this.getCssClass() + " scrubber")
					.slider(this.getSliderConfig());
				// Up the z-index of the default status indicator:
				this.$el.find('.ui-slider-handle')
					.addClass('playHead PIE btn')
					.wrap('<div class="handle-wrapper" />');
				// Update attributes: 
				this.updateAttr({ 'value': 0 });

				this.$el.find('.ui-slider-range-min').addClass('watched');
				// Add buffer:
				this.$el.append(
					$('<div />').addClass("buffered")
				);
				// if parent is controlsContainer set to zero width and update at update layout time.
				if (this.getConfig('parent') == 'controlsContainer') {
					this.$el.css({
						'width': this.getConfig('minWidth')
					});
				}
			}
			return this.$el;
		}
	}));

})(window.mw, window.jQuery, kWidget);
