(function (mw, $, kWidget) {
    "use strict";

    mw.PluginManager.add('audioSelector', mw.KBaseComponent.extend({

        defaultConfig: {
            "parent": "controlsContainer",
            "order": 61,
            "displayImportance": 'low',
            "align": "right",
            "iconClass": "icon-audio",
            "showTooltip": true,
            "labelWidthPercentage": 33,
            "defaultStream": 1,
            "maxNumOfStream": 4,
            "enableKeyboardShortcuts": true,
            "keyboardShortcutsMap": {
                "nextStream": 221,   // Add ] Sign for next stream
                "prevStream": 219,   // Add [ Sigh for previous stream
                "defaultStream": 220 // Add \ Sigh for default stream
            },
            defaultLanguage: undefined
        },

        isDisabled: false,
        streams: [],
        streamsReady: false,
        streamEnded: false,
        userLangCode: undefined,

        setup: function () {
            var _this = this;
            this.userLangCode = this.getConfig('defaultLanguage');
            if (this.userLangCode != undefined) {
                this.userLangCode = this.userLangCode.slice(0, 2);
            }
            mw.defaultLanguage = this.userLangCode;
            this.addBindings();
            // update styles after resizing the window
            // height offset is not changed right after getting resize event
            // that's why player wait 190ms before setting styles
            $(window).resize(function () {
                setTimeout(function () {
                    _this.setStyles();
                }, 190);
            });
        },

        /**
         * function find the native name for the language by code
         * @param code - code for the language we need
         * @param orgName - orginal name of the language in case if the code wont be in the list
         * @returns nativeName of the language
         */
        getLanguageNativeNameByCode: function (code, orgName) {
            if (mw.Language && mw.Language.names[code]) {
                return mw.Language.names[code];
            }
            // if there is no native language for the selected code return original name
            return orgName;
        },

        /**
         * retu
         * @param code
         * @returns {*}
         */
        getSortIndex: function (code) {
            if (mw.Language && mw.Language.order[code]) {
                return mw.Language.order[code];
            }
            // if lang is original (code == --) return 0 to put it on the top
            // else if lang is out of EU languages put it to the bottom of EU langs
            return code == "--" ? 0 : Object.keys(mw.Language.order).length;
        },

        /**
         * function return stream object by code
         * @param code - code for the language we need
         * @returns function return stream object by code
         */
        getStreamByCode: function (code) {
            for (var i = 0; i < this.streams.length; i++) {
                if (this.streams[i].code == code)
                    return this.streams[i];
            }
            return false;
        },

        destroy: function () {
            this._super();
            this.getComponent().remove();
        },
        dynamicSort: function (property) {
            var sortOrder = 1;
            if (property[0] === "-") {
                sortOrder = -1;
                property = property.substr(1);
            }
            return function (a, b) {
                var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
                return result * sortOrder;
            }
        },
        addBindings: function () {
            var _this = this;

            this.bind('playerReady', function () {
                if (!_this.streamsReady) {
                    _this.onDisable();
                    _this.getPlayer().triggerHelper("updatePropertyEvent", {
                        "plugin": _this.pluginName,
                        "property": "sources",
                        "items": [{'label': gM("mwe-timedtext-no-audio"), 'value': gM("mwe-timedtext-no-audio")}],
                        "selectedItem": gM("mwe-timedtext-no-audio")
                    });
                }
            });

            this.bind('audioTracksReceived', function (e, data) {
                if (data.languages && data.languages.length > 1) {

                    // sort languages by code
                    _this.streams = data.languages.sort(_this.dynamicSort("code"));

                    // get native languages by codes
                    $.each(data.languages, function (ind, lang) {
                        if (lang.label == "--")
                            lang.label = "Original";
                        lang.label = _this.getLanguageNativeNameByCode(lang.code, lang.label);
                        lang.sortIndex = _this.getSortIndex(lang.code);
                    });

                    // sort by sort index to devide EU langs from the rest
                    _this.streams = data.languages.sort(_this.dynamicSort("sortIndex"));
                    var defaultStream = null;
                    if (_this.userLangCode != undefined) {
                        defaultStream = _this.getStreamByCode(_this.userLangCode);
                        _this.embedPlayer.sendNotification("changeAudioHLS", defaultStream);
                    }
                    if (!defaultStream)
                        defaultStream = _this.getDefaultStream();
                    _this.setStream(defaultStream);
                    _this.currentLabel = defaultStream.label;
                    _this.setLabel();
                    _this.getComponent().find("ul").show();
                    _this.buildMenu();
                    _this.setStyles();
                    _this.streamsReady = true;
                    _this.onEnable();
                } else {
                    _this.hide()
                }
            });

            this.bind('audioTrackIndexChanged', function (e, arg) {
                _this.setLabel();
                _this.externalSetStream(arg);
            });

            if (this.getConfig('enableKeyboardShortcuts')) {
                this.bind('addKeyBindCallback', function (e, addKeyCallback) {
                    _this.addKeyboardShortcuts(addKeyCallback);
                });
            }

            this.embedPlayer.bindHelper("propertyChangedEvent", function (event, data) {
                if (data.plugin === _this.pluginName) {
                    if (data.property === "sources") {
                        _this.getMenu().$el.find("li a")[data.value].click();
                    }
                }
            });
        },

        /**
         * set label text on control bar eleement
         */
        setLabel: function () {
            var _this = this;
            var appendStyle = '<style>.icon-audio::before{content: "' + _this.currentLabel + '" !important;}</style>';
            $('head').append(appendStyle);
            _this.onEnable();
            _this.getBtn()[0].style.width = "auto";
            _this.getBtn()[0].style['margin-top'] = '-1px';
            _this.getBtn()[0].style['font'] = 'normal 15px helvetica, arial, sans-serif';
        },

        /**
         * set style of the drop down menu according to the player size
         * prevent cutting the languages and bitrates in menusk
         */
        setStyles: function () {
            this.css_getclass('.dropup .dropdown-menu, .navbar-fixed-bottom .dropdown .dropdown-menu').style['max-height'] = String(this.embedPlayer.offsetHeight - 20) + "px";
        },

        /**
         * function search all css rules
         * @returns css rules for the document
         */
        cssrules: function () {
            var rules = {};
            var ds = document.styleSheets, dsl = ds.length;
            for (var i = 0; i < dsl; ++i) {
                var dsi = ds[i].cssRules, dsil = dsi ? dsi.length : 0;
                for (var j = 0; j < dsil; ++j) rules[dsi[j].selectorText] = dsi[j];
            }
            return rules;
        },

        /**
         * function search for the specific css class in all css rules of the document
         * @param name - name of the searching css class
         * @param createifnotfound - flag deciding if create css class if not found
         * @returns css class
         */
        css_getclass: function (name, createifnotfound) {
            var rules = this.cssrules();
            if (!rules.hasOwnProperty(name)) throw 'todo:deal_with_notfound_case';
            return rules[name];
        },

        addKeyboardShortcuts: function (addKeyCallback) {
            var _this = this;
            // Add ] Sign for next stream
            addKeyCallback(this.getConfig("keyboardShortcutsMap").nextStream, function () {
                _this.setStream(_this.getNextStream());
            });
            // Add [ Sigh for previous stream
            addKeyCallback(this.getConfig("keyboardShortcutsMap").prevStream, function () {
                _this.setStream(_this.getPrevStream());
            });
            // Add \ Sigh for default stream
            addKeyCallback(this.getConfig("keyboardShortcutsMap").defaultStream, function () {
                _this.setStream(_this.getDefaultStream());
            });
        },
        getNextStream: function () {
            if (this.streams[this.getCurrentStreamIndex() + 1]) {
                return this.streams[this.getCurrentStreamIndex() + 1];
            }
            return this.streams[this.getCurrentStreamIndex()];
        },
        getPrevStream: function () {
            if (this.streams[this.getCurrentStreamIndex() - 1]) {
                return this.streams[this.getCurrentStreamIndex() - 1];
            }
            return this.streams[this.getCurrentStreamIndex()];
        },
        getDefaultStream: function () {
            return this.streams[(this.getConfig('defaultStream') - 1)];
        },
        getCurrentStreamIndex: function () {
            var _this = this;
            var index = null;
            $.each(this.streams, function (idx, stream) {
                if (_this.currentStream == stream) {
                    index = idx;
                    return false;
                }
            });
            return index;
        },
        buildMenu: function () {
            var _this = this;
            if (!this.currentLabel) {
                var appendStyle = '<style>.icon-audio::before{content: "Original" !important;}</style>';
                $('head').append(appendStyle);
            }
            _this.getBtn()[0].style.width = "auto"
            _this.getBtn()[0].style['margin-top'] = '-1px';
            _this.getBtn()[0].style['font'] = 'normal 15px helvetica, arial, sans-serif';

            // Destroy old menu
            this.getMenu().destroy();

            if (!this.streams.length) {
                this.log("Error with getting streams");
                //this.destroy();
                return;
            }
            var items = [];
            $.each(this.streams, function (streamIndex, stream) {
                if (stream.label != "unknown") {
                    _this.addStreamToMenu(streamIndex, stream);
                    items.push({'label': stream.label, 'value': stream.label});
                }
            });
            var actualWidth = this.getMenu().$el.width();
            var labelWidthPercentage = parseInt(this.getConfig("labelWidthPercentage")) / 100;
            var labelWidth = this.getPlayer().getWidth() * labelWidthPercentage;
            if (actualWidth > labelWidth) {
                this.getMenu().$el.find('a').width(labelWidth);
            }

            var ind = this.getCurrentStreamIndex() == null ? 0 : this.getCurrentStreamIndex();
            this.getMenu().setActive(ind);
            // dispatch event to be used by a master plugin if defined
            this.getPlayer().triggerHelper("updatePropertyEvent", {
                "plugin": this.pluginName,
                "property": "sources",
                "items": items,
                "selectedItem": this.getMenu().$el.find('.active a').text()
            });

        },
        addStreamToMenu: function (id, stream) {
            var _this = this;
            var active = (this.getCurrentStreamIndex() == stream.index);

            this.getMenu().addItem({
                'label': stream.label,
                'attributes': {
                    'id': stream.index,
                    'padding-right': '20px'
                },
                'callback': function () {
                    _this.setStream(stream);
                    var isNativeSDK = mw.getConfig("EmbedPlayer.ForceNativeComponent");
                    if (!isNativeSDK) {
                        _this.getBtn()[0].style['font-family'] = 'icomoon';
                        _this.getBtn()[0].style['margin-top'] = '0px';
                        var appendStyle = '<style>.icon-audio::before{content: "\\e627" !important;}</style>';
                        $('head').append(appendStyle);
                    }
                    _this.embedPlayer.sendNotification("changeAudioHLS", stream);
                },
                'active': active
            });
            this.getMenu().$el.find("a").addClass("truncateText");
        },
        externalSetStream: function (id) {
            var stream = this.streams[id];
            if (stream) {
                this.setStream(stream);
            } else {
                this.log("Error - invalid stream id");
            }
        },
        setStream: function (stream) {
            if (this.currentStream == stream || (this.currentStream == undefined && (stream.label.toUpperCase() == "ORIGINAL" || stream.label == "--")))
                return;
            //this.onDisable();
            this.currentStream = stream;
            this.embedPlayer.triggerHelper('switchAudioTrack', {index: stream.index});
            if (stream.label) {
                this.currentLabel = stream.label;
                $('head').append('<style>.icon-audio::before{content: "' + stream.label + '" !important;}</style>')
            }
            var isNativeSDK = mw.getConfig("EmbedPlayer.ForceNativeComponent");
            if (!isNativeSDK)
                this.getComponent().find('button').addClass('in-progress-state-audio');
        },
        toggleMenu: function () {
            if (this.isDisabled) {
                return;
            }
            this.getMenu().toggle();
        },
        getComponent: function () {
            var _this = this;
            if (!this.$el) {
                var $menu = $('<ul />').hide();
                //TODO: need icon from Shlomit (Csabit)!
                var $button = $('<button />')
                    .addClass('btn icon-audio')
                    .attr('title', gM('mwe-embedplayer-select_audio'))
                    .click(function (e) {
                        _this.toggleMenu();
                    });
                this.setAccessibility($button, gM('mwe-embedplayer-select_audio'));
                this.$el = $('<div />')
                    .addClass('dropup' + this.getCssClass())
                    .append($button, $menu);
            }
            if (_this.streams == undefined || _this.streams.length == 0) {
                this.$el[0].children[0].title = '';
                this.embedPlayer.setKalturaConfig("audioSelector", 'showTooltip', false);
            } else {
                this.$el[0].children[0].title = gM('mwe-embedplayer-select_audio')
                this.embedPlayer.setKalturaConfig("audioSelector", 'showTooltip', true);
            }
            return this.$el;
        },
        getMenu: function () {
            if (!this.menu) {
                this.menu = new mw.KMenu(this.getComponent().find('ul'), {
                    tabIndex: this.getBtn().attr('tabindex')
                });
            }
            return this.menu;
        },
        getBtn: function () {
            return this.getComponent().find('button');
        },
        onEnable: function () {
            this.getComponent().find('button').removeClass('in-progress-state-audio');
            this.isDisabled = false;
            this.updateTooltip(gM('mwe-embedplayer-select_audio'));
            this.getBtn().removeClass('disabled');
        },
        onDisable: function () {
            this.updateTooltip(gM('mwe-embedplayer-switch_stream'));
            this.getComponent().removeClass('open');
            if (this.embedPlayer.plugins.hlsjs) return;
            //todo: add animation while language is switching
            this.isDisabled = true;
            this.getBtn().addClass('disabled');
        }
    }));

})(window.mw, window.jQuery, kWidget);
