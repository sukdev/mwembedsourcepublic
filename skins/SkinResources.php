<?php

// go over the folder list 
return array(
	'kdark' => array(
		/** 
		  * We need to have mw.EmbedPlayer dependency for our skin
		  * So that the Core CSS will load before Skin CSS
		 **/
		'dependencies' => 'mw.EmbedPlayer',
		'styles' => array(
            'skins/streamUK/css/layout.css',
            'skins/streamUK/css/icons.css',
		)
	)
);