(function (mw, $) {
	"use strict";

	mw.PluginManager.add('currentTimeLabel', mw.KBaseComponent.extend({

		defaultConfig: {
			"parent": "controlsContainer",
			"order": 21,
			"displayImportance": "high",
            "timeDisplayPattern": "0:00",
			"countDownMode": false
		},

		updateEnabled: true,
		labelWidth: null,
        seekState:false,

		setup: function () {
			var _this = this;
			if ( this.embedPlayer.isMobileSkin() ){
				this.setConfig("order", 26);
				this.setConfig("countDownMode", true);
			}
			this.bindTimeUpdate();
			this.bind('externalTimeUpdate', function (e, newTime) {
				if (newTime != undefined) {
					_this.updateUI(newTime);
				}
			});
			// zero the current time when changing media
			this.bind('onChangeMediaDone', function () {
                _this.updateUI(0);
            });

            this.embedPlayer.addJsListener('doSeek', function () {
                _this.seekState = true;
            });
            this.embedPlayer.addJsListener('playerSeekEnd', function () {
                _this.seekState = false;
            });

			//will stop listening to native timeupdate events
			this.bind('detachTimeUpdate', function () {
				_this.unbind('timeupdate');
			});
			//will re-listen to native timeupdate events
			this.bind('reattachTimeUpdate', function () {
				_this.bindTimeUpdate();
			});
			// Bind to Ad events
			this.bind('AdSupport_AdUpdatePlayhead', function (e, currentTime) {
				if (_this.getPlayer().isInSequence()) {
					_this.updateUI(currentTime);
				}
			});
			this.bind('AdSupport_EndAdPlayback', function () {
				_this.updateUI(_this.getCurrentTime());
			});
			this.bind('seeked', function () {

                //console.log("seekend CURRENT TIME LABEL = ", _this.getCurrentTime())

                if (!_this.block) {
				_this.updateUI(_this.getCurrentTime());
                }
			});
			this.bind("freezeTimeIndicators", function (e, state) {
				if (state === true) {
					_this.updateEnabled = false;
				} else {
					_this.updateEnabled = true;
				}
			});
		},
        block:false,
		bindTimeUpdate: function () {
			var _this = this;
			this.bind('timeupdate', function () {
                if (!_this.block) {
				if (!_this.getPlayer().isInSequence()) {
					_this.updateUI(_this.getCurrentTime());
				}
                }
			});

            this.bind('timeupdate2', function (callBackData, data) {
                _this.block = true;
                if (!_this.getPlayer().isInSequence()) {
                    _this.updateUI(data);
                }
            });



		},
		updateUI: function (time) {
            if(this.seekState == true)
                return;
			if (this.updateEnabled) {
				if (this.getConfig("countDownMode")){
					time = this.embedPlayer.getDuration() - time;
				}
				this.getComponent().text(mw.seconds2npt(time));
				// check if the time change caused the label width to change (got to 10 minutes or 1 hour) and recalculate components position if needed
				var currentWidth = this.$el.width();
				if ( currentWidth !== this.labelWidth ){
					this.embedPlayer.layoutBuilder.updateComponentsVisibility();
					this.labelWidth = currentWidth;
				}
			}
		},
		getCurrentTime: function () {
			var ct = this.getPlayer().getPlayerElementTime() - this.getPlayer().startOffset;
			if (ct < 0) {
				ct = 0;
			}
			return parseFloat(ct);
		},
		getComponent: function () {
			if (!this.$el) {
				this.$el = $('<div />')
					.addClass("timers" + this.getCssClass())
                    .text(this.getConfig('timeDisplayPattern'));
			}
			if (this.getConfig("countDownMode")){
				this.$el.text(mw.seconds2npt(this.embedPlayer.getDuration()));
			}
			this.labelWidth = this.$el.width();
			return this.$el;
		},
		show: function () {
			this.getComponent().css('display', 'inline').removeData('forceHide');
		}
	}));

})(window.mw, window.jQuery);