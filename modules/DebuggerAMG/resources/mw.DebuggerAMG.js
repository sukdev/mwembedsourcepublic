/**
 * onscreen debugger
 */
(function (mw, $) {
    "use strict";
    mw.PluginManager.add('debuggerAMG', mw.KBaseComponent.extend({
        defaultConfig: {
            parent: "videoHolder",
            order: 62,
            displayImportance: 'low',
            align: "right",
            filter: null,
            exclude: null
        },
        icons: {
            'mute': 'icon-volume-mute',
            'high': 'icon-volume-high'
        },
        /**
         * setup plugin
         */
        setup: function () {
            mw.debuggerAMG = this;
            mw.log = this.screenLogger;
        },

        screenLogger: function (msg) {
            // dont show external time updates
            var filt = mw.getConfig("filter");
            if (filt == null || msg.indexOf(filt) > -1) {
                if (msg == "KDPMapping:: sendNotification > externalTimeUpdate" || msg == "Error - you must implement getComponent in your plugin:statisticsSUK")return;
                mw.debuggerAMG.getComponent()[0].value += "\n" + JSON.stringify(msg);
                mw.debuggerAMG.getComponent()[0].scrollTop += mw.debuggerAMG.getComponent()[0].scrollHeight;
            } else
                return;
        },

        copyText: function (e) {
            document.execCommand('copy');
        },

        /**
         * returnning the component, if not existing it creates it
         * @returns {*}
         */
        getComponent: function () {
            var _this = this;
            if (!this.$el) {
                this.$el = $("<textarea id='info_overlay' onclick='this.focus();this.select();' style='color:white;  background:#000000; overflow:scroll; overflow-y: scroll; opacity:0.5; position:absolute; width:100%; height:100%'></textarea>")
                    .click(function (e) {
                        _this.copyText(e);
                    });
            }
            return this.$el;
        }

    }));

})(window.mw, window.jQuery);