(function (mw, $) {
    "use strict";
    // initialize plugin only for android devices
    if (mw.isAndroid())
        mw.PluginManager.add('volumeMuteAMG', mw.KBaseComponent.extend({
            // plugin shows the mute / unmute icon for android devices
            // hardware buttons are not handled by javascript and when player starts on mute its not possible
            // to turn on the voice - this plugin fix it
            defaultConfig: {
                parent: "controlsContainer",
                order: 62,
                displayImportance: 'low',
                align: "right"
            },
            icons: {
                'mute': 'icon-volume-mute',
                'high': 'icon-volume-high'
            },
            /**
             * setup plugin
             * @param embedPlayer
             */
            setup: function (embedPlayer) {
                var _this = this;

                var muted = _this.embedPlayer.muted ? 0 : 1;
                _this.updateVolumeUI(muted);
                if (_this.embedPlayer.muted) _this.embedPlayer.setVolume(muted);

                // Add click bindings, on click change mute unmute
                this.getBtn().click(function () {
                    var newVolumeValue = _this.embedPlayer.volume == 0 ? 1 : 0;
                    _this.embedPlayer.setVolume(newVolumeValue);
                    _this.updateVolumeUI(newVolumeValue);
                });
            },

            /**
             * updating the icon (mute or unmute)
             * @param volume
             */
            updateVolumeUI: function (volume) {
                var iconClasses = '',
                    newClass = '';

                // Get all icons classes
                $.each(this.icons, function () {
                    iconClasses += this + ' ';
                });

                // Select icon class based on volume percent
                if (volume == 0) {
                    newClass = this.icons['mute'];
                } else {
                    newClass = this.icons['high'];
                }

                // Remove all icon classes and add new one
                this.getBtn().removeClass(iconClasses).addClass(newClass);
            },

            /**
             * returnning the component, if not existing it creates it
             * @returns {*}
             */
            getComponent: function () {
                var _this = this;
                if (!this.$el) {

                    var $btn = $('<button />')
                        .addClass("btn " + this.icons['high'])
                        .attr({'title': gM('mwe-embedplayer-volume-mute'), 'id': 'muteBtn'});

                    this.$el = $('<div />')
                        .addClass('comp volumeControl pull-right horizontal')
                        .append(
                        $btn
                    );
                }

                //.addClass('comp volumeControl pull-right horizontal')
                return this.$el;
            },

            /**
             * returnning mute button
             * @returns {*}
             */
            getBtn: function () {
                return this.getComponent().find('#muteBtn');
            }

        }));

})(window.mw, window.jQuery);