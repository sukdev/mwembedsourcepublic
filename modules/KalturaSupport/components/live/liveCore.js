( function( mw, $ ) {"use strict";

	mw.PluginManager.add( 'liveCore', mw.KBasePlugin.extend({

		firstPlay : false,
		/**
		 * API requests interval for updating live stream status (Seconds).
		 * Default is 30 seconds, to match server's cache expiration
		 */
		liveStreamStatusInterval : 30,

		// Default DVR Window (Seconds)
		defaultDVRWindow : 30 * 60,

		onAirStatus: true,

		defaultConfig: {
			//time in ms to wait before displaying the offline alert
			offlineAlertOffest: 1000,
			//disable the islive check (force live to true)
			disableLiveCheck: false,
			//hide live indicators when playing offline from DVR
			hideOfflineIndicators: false,
			//hide currentTimeLabel when livestream is not DVR
			hideCurrentTimeLabel: true,
            //show thumbnail if livestream becomes offline
            showThumbnailWhenOffline : false
		},

		/**
		 * (only for iOS) indicates we passed the dvr window size and once we will seek backwards we should reAttach timUpdate events
		 */
		shouldReAttachTimeUpdate: false,

		playWhenOnline:false,

		/**
		 * In native HLS playback we don't get duration so we set it to maximum "currentTime" value
		 */
		maxCurrentTime: 0,

		/**
		 * indicates that we've received the first live status update
		 */
		liveStreamStatusUpdated : false,
        firstTimePlay:true,

		setup: function() {
			this.addPlayerBindings();
			this.extendApi();
		},

        /**
         * if error while media is recorded
         * @param errorId
         */
        rebootMediaOnError: function(errorId){
            var _this = this;
            if (_this.embedPlayer.getFlashvars('recorded') && errorId == 16){
                _this.embedPlayer.unbindHelper('embedPlayerError');
                _this.embedPlayer.stop();
                _this.embedPlayer.sendNotification('changeMedia',{'entryId':_this.embedPlayer.kentryid});
                _this.firstTimePlay = true;
                _this.stayPaused = true;
            }
        },


        /**
		 * Extend JS API to match the KDP
		 */
		extendApi: function() {
			var _this = this;

			this.getPlayer().isOffline = function() {
				return !_this.onAirStatus;
			}
		},

		addPoster: function(){
            this.getPlayer().removePosterFlag = false;
			this.getPlayer().updatePosterHTML();
		},

		removePoster: function(){
			this.getPlayer().removePoster();
			this.getPlayer().removePosterFlag = true;
		},

		addPlayerBindings: function() {
			var _this = this;
			var embedPlayer = this.getPlayer();

			this.bind( 'checkIsLive', function( e, callback ) {
				_this.getLiveStreamStatusFromAPI( callback );
			});

            embedPlayer.addJsListener('doPlay', function () {
                if (_this.embedPlayer.getFlashvars('recorded') == true && _this.firstTimePlay == true) {
                    _this.addPoster();
                }
            });

            /**
             * handle case  when live stream changed to recorded
             * play from beginning as VOD
             * if duration is not fitting recorded lenght handle embed player error
             */
            embedPlayer.addJsListener('playing', function () {
                if (_this.embedPlayer.getFlashvars('recorded') == true) {
                    _this.embedPlayer.unbindHelper('embedPlayerError');
                    _this.embedPlayer.bindHelper('embedPlayerError', function(e, data) {
                        _this.rebootMediaOnError(data.errorId);
                    });
		    //Android gets notifications sometimes doesnt get same order
		    //Can happen on all devices cause there is no garantee that a notifications gets in first
                    if(mw.isAndroid())
			return;	
                    if (_this.firstTimePlay == true) {
                        _this.addPoster();
                        _this.firstTimePlay = false;
                        _this.embedPlayer.sendNotification("doStop");
                        setTimeout(function () {
                            _this.embedPlayer.sendNotification("doSeek", .3);
                            setTimeout(function () {
                                _this.embedPlayer.sendNotification("doPlay");
                            }, 20);
                        }, 20);
                    }else{
                        if(_this.stayPaused){
                            _this.stayPaused = false;
                            _this.embedPlayer.sendNotification("doStop");
                        }
                        _this.removePoster();
                    }
                }
            });


            this.bind( 'playerReady', function() {
				_this.onAirStatus = true;
				_this.isLiveChanged();
			} );

			this.bind( 'onpause', function() {
				if ( embedPlayer.isLive() && _this.isDVR() && _this.switchDone ) {
					embedPlayer.addPlayerSpinner();
					_this.getLiveStreamStatusFromAPI( function( onAirStatus ) {
						if ( onAirStatus ) {
                            //SUK dont increase scrubber when its recorded
							if ( _this.shouldHandlePausedMonitor() && !_this.embedPlayer.playerConfig.vars.recorded ) {
								_this.addPausedMonitor();
							}
						}
					} );
				}
			} );

			this.bind( 'firstPlay', function() {
				_this.firstPlay = true;
			} );

			this.bind( 'AdSupport_PreSequenceComplete', function() {
				_this.switchDone = true;
			} );

			this.bind( 'liveStreamStatusUpdate', function( e, onAirObj ) {

				if ( !_this.liveStreamStatusUpdated ) {
					_this.liveStreamStatusUpdated = true;
					if( onAirObj.onAirStatus ){
						_this.addPoster();
						_this.getPlayer().enablePlayControls();
					}else{
						_this.getPlayer().disablePlayControls();
					}
				}

				//if we moved from live to offline  - show message
				if ( _this.onAirStatus && !onAirObj.onAirStatus ) {
                    //calculate offlineAlertOffset for timeout (by default = 0 as sometimes offline is only for a second and the message is not needed..)
                    var firstOfflineAlertOffest = _this.calculateOfflineAlertOffest();
                    _this.setOffAir(firstOfflineAlertOffest);
				}  else if ( !_this.onAirStatus && onAirObj.onAirStatus ) {
                    if (_this.offAirTimeout){
                        clearTimeout(_this.offAirTimeout);
                        _this.offAirTimeout = null;
                    }
					if ( _this.getPlayer().removePosterFlag && !_this.playWhenOnline && !embedPlayer.isPlaying() ) {
						_this.addPoster();
					}
                    embedPlayer.layoutBuilder.closeAlert(); //moved from offline to online - hide the offline alert
					if ( !_this.getPlayer().getError() ) {
						_this.getPlayer().enablePlayControls();
					}
                    embedPlayer.triggerHelper( 'liveOnline' );
					if ( _this.playWhenOnline ) {
						embedPlayer.play();
						_this.playWhenOnline = false;
					}

					//reload livestream
					if ( !embedPlayer.firstPlay && _this.isDVR() ) {
						embedPlayer.disablePlayControls();
						var shouldPause = !embedPlayer.isPlaying();
						var playingEvtName = "playing.backToLive";
						embedPlayer.bindHelper( playingEvtName , function() {
							embedPlayer.unbindHelper( playingEvtName );
							setTimeout( function() {
								embedPlayer.enablePlayControls();
								if ( shouldPause ) {
									embedPlayer.pause();
								}
							}, 1);

						});

						setTimeout( function() {
							_this.maxCurrentTime = 0;
							//in case player was in 'ended' state change to 'paused' state
							embedPlayer.pauseInterfaceUpdate();
							embedPlayer.backToLive();
						}, 1000 );

					}
				}

				//check for pending autoPlay
				if ( onAirObj.onAirStatus &&
					embedPlayer.firstPlay &&
					embedPlayer.autoplay &&
					embedPlayer.canAutoPlay() &&
					!embedPlayer.isInSequence() &&
					!embedPlayer.isPlaying() ) {
					embedPlayer.play();
				}

				_this.onAirStatus = onAirObj.onAirStatus;

				if ( _this.isDVR() ) {
					if ( !onAirObj.onAirStatus ) {
						if ( _this.shouldHandlePausedMonitor() ) {
							_this.removePausedMonitor();
						}
					}
				}
			} );

			this.bind( 'durationChange', function( e, newDuration) {
				if ( _this.switchDone && embedPlayer.isLive() && _this.isDVR() && embedPlayer.paused ) {
					//refresh playhead position
					embedPlayer.triggerHelper( 'timeupdate', [ embedPlayer.getPlayerElementTime() ] );
					embedPlayer.triggerHelper( 'updatePlayHeadPercent', [ embedPlayer.getPlayerElementTime() / embedPlayer.duration ] );

				}
			});

			this.bind( 'liveEventEnded', function() {
				if ( embedPlayer.isLive() && _this.isDVR() ) {
					//change state to "VOD"
					embedPlayer.setLive( false );
					if ( _this.getConfig('hideOfflineIndicators') ) {
						_this.isLiveChanged();
					}
					//once moving back to live, set live state again
					embedPlayer.bindHelper( 'liveOnline', function() {
						embedPlayer.setLive( true );
					} );

					if ( !_this.isNativeHLS() ) {
						embedPlayer.bindHelper( 'ended', function() {
							embedPlayer.getPlayerElement().seek( 0 );
						});
					}
				}
			});

			this.bind( 'movingBackToLive', function() {
				//in case stream is shorter now (long disconnection) reset the duration
				 if ( _this.isDVR() && _this.isNativeHLS() ) {
					 _this.maxCurrentTime = 0;
				 }
			});
		},

        setOffAir: function(offlineAlertOffest) {
            var _this = this;
            var embedPlayer = this.getPlayer();

            if (_this.offAirTimeout){
                clearTimeout(_this.offAirTimeout);
                _this.offAirTimeout = null;
            }

            _this.offAirTimeout = setTimeout( function() {
                if ( !_this.onAirStatus ) {
                    //recheck the buffer length
                    var secondOfflineAlertOffest = _this.calculateOfflineAlertOffest();
                    if( secondOfflineAlertOffest > 1000 ) {
                        _this.setOffAir(secondOfflineAlertOffest);
                    } else {
                        //if we already played once it means stream data was loaded. We can continue playing in "VOD" mode
                        if (!embedPlayer.firstPlay && _this.isDVR()) {
                            embedPlayer.triggerHelper('liveEventEnded');
                        } else {
                            //remember last state
                            _this.playWhenOnline = embedPlayer.isPlaying();

                            if (_this.getConfig('showThumbnailWhenOffline')) {
                                _this.addPoster();
                            } else {
                                embedPlayer.layoutBuilder.displayAlert({
                                    title: embedPlayer.getKalturaMsg('ks-LIVE-STREAM-OFFLINE-TITLE'),
                                    message: embedPlayer.getKalturaMsg('ks-LIVE-STREAM-OFFLINE'),
                                    keepOverlay: true,
                                    noButtons: true,
                                    props: {
                                        customAlertTitleCssClass: "AlertTitleTransparent",
                                        customAlertMessageCssClass: "AlertMessageTransparent",
                                        customAlertContainerCssClass: "AlertContainerTransparent"
                                    }
                                });
                            }

                            _this.getPlayer().disablePlayControls();
                        }
                        embedPlayer.triggerHelper('liveOffline');
                    }
                }
            }, offlineAlertOffest );
        },

		isLiveChanged: function() {
			var _this = this;
			var embedPlayer = this.getPlayer();

			//ui components to hide
			var showComponentsArr = [];
			//ui components to show
			var hideComponentsArr = [];
			_this.maxCurrentTime = 0;
			_this.liveStreamStatusUpdated = false;
			//live entry
			if ( embedPlayer.isLive() ) {
				if ( !this.getConfig("disableLiveCheck")) {
					//the controls will be enabled upon liveStatus==true notification
					_this.removePoster();
					embedPlayer.disablePlayControls();
				}
				_this.addLiveStreamStatusMonitor();
				//hide source selector until we support live streams switching
				hideComponentsArr.push( 'sourceSelector' );
				embedPlayer.addPlayerSpinner();
				_this.getLiveStreamStatusFromAPI( function( onAirStatus ) {
					if ( !embedPlayer._checkHideSpinner ) {
						embedPlayer.hideSpinner();
					}
				} );
				_this.switchDone = true;
				if ( embedPlayer.sequenceProxy ) {
					_this.switchDone = false;
				}

				hideComponentsArr.push( 'durationLabel' );
				//live + DVR
				if ( _this.isDVR() ) {
					_this.dvrWindow = embedPlayer.evaluate( '{mediaProxy.entry.dvrWindow}' ) * 60;
					if ( !_this.dvrWindow ) {
						_this.dvrWindow = _this.defaultDVRWindow;
					}
					showComponentsArr.push( 'scrubber' );
                    // SUK: no new DVR layout - this is how we roll
                    // hideComponentsArr.push( 'currentTimeLabel' ); //new DVR layout: no time label, only negative live edge offset at the mousemove over the scrubber
				} else {  //live + no DVR
					showComponentsArr.push( 'liveStatus' );
                    hideComponentsArr.push( 'scrubber', 'currentTimeLabel' );
				}

				if ( _this.isNativeHLS() ) {
					_this.bind( 'timeupdate' , function() {
						var curTime = embedPlayer.getPlayerElementTime();

						if ( _this.isDVR() ) {
						  if ( curTime > _this.maxCurrentTime ) {
							_this.maxCurrentTime = curTime;
							//embedPlayer.setDuration( _this.maxCurrentTime );
                            // T20160404.0018-RTE-Recorded_DVR_playing_as_live_on_Safari_Mac_Scrubber_Broken_Duration_Incorrect
                            // FIX - when we have VOD made from live dvr ("recorded") don't update duration since its
                            // setting total duration to current time (acting like live video with dvr) - bug only on Safari MAC
                            if ((mw.isDesktopSafari() || mw.isIpad()) && embedPlayer.isDVR() == true)
                              embedPlayer.setDuration( _this.maxCurrentTime );
						  }
						}
					});
				}

				if ( _this.shouldHandlePausedMonitor() ) {
					_this.bind( 'playing', function() {
						if ( _this.isDVR() && _this.switchDone ) {
							//	_this.hideLiveStreamStatus();
							_this.removePausedMonitor();
						}
					} );
				}
			}
			//not a live entry: restore ui, hide live ui
			else {
				embedPlayer.removePosterFlag = false;
				hideComponentsArr.push( 'liveStatus' );
				showComponentsArr.push( 'sourceSelector', 'scrubber', 'durationLabel', 'currentTimeLabel' );
				_this.removeLiveStreamStatusMonitor();
				_this.unbind('timeupdate');
			}

			embedPlayer.triggerHelper('onShowInterfaceComponents', [ showComponentsArr ] );
			embedPlayer.triggerHelper('onHideInterfaceComponents', [ hideComponentsArr ] );
			embedPlayer.doUpdateLayout();
		},

		isDVR: function(){
			return ( this.getPlayer().isDVR()  && this.getPlayer().isTimeUpdateSupported() );
		},

		getCurrentTime: function() {
			return this.getPlayer().getPlayerElement().currentTime;
		},

		removeMinDVRMonitor: function() {
			this.log( "removeMinDVRMonitor" );
			this.minDVRMonitor = clearInterval( this.minDVRMonitor );
		},

		/**
		 * API Requests to update on/off air status
		 */
		addLiveStreamStatusMonitor: function() {
			//if player is in error state- no need for islive calls
			if ( this.embedPlayer.getError() ) {
				return;
			}
			this.log( "addLiveStreamStatusMonitor" );
			var _this = this;
			this.liveStreamStatusMonitor = setInterval( function() {
				_this.getLiveStreamStatusFromAPI();
			}, _this.liveStreamStatusInterval * 1000 );
		},

		removeLiveStreamStatusMonitor: function() {
			this.log( "removeLiveStreamStatusMonitor" );
			this.liveStreamStatusMonitor = clearInterval( this.liveStreamStatusMonitor );
		},

		/**
		 * indicates if we should handle paused monitor.
		 */
		shouldHandlePausedMonitor: function() {
			if ( this.isNativeHLS() ) {
				return true;
			}
			return false;
		},

		/**
		 * Updating display time & scrubber while in paused state
		 */
		addPausedMonitor: function() {
			var _this = this;
			var embedPlayer = this.embedPlayer;
			var pauseTime = _this.maxCurrentTime;
			if ( pauseTime == 0 ) {
				pauseTime = embedPlayer.getPlayerElementTime();
			}
			var pauseClockTime = Date.now();
			this.log( "addPausedMonitor :   Monitor rate = " + mw.getConfig( 'EmbedPlayer.MonitorRate' ) );
			this.pausedMonitor = setInterval( function() {
				var timePassed = ( Date.now() - pauseClockTime ) / 1000;
				embedPlayer.setDuration( pauseTime + timePassed );
			}, 1000 );
		},

		removePausedMonitor: function() {
			this.log( "removePausedMonitor" );
			this.pausedMonitor = clearInterval( this.pausedMonitor );
		},

		/**
		 * Get on/off air status based on the API and update locally
		 */
		getLiveStreamStatusFromAPI: function( callback ) {
			var _this = this;
			var embedPlayer = this.getPlayer();

			if ( embedPlayer.getFlashvars( 'streamerType') == 'rtmp' ) {
				if ( callback ) {
					callback( _this.onAirStatus );
				}
				return;
			}

			if (this.getConfig("disableLiveCheck")){
				if ( callback ) {
					callback( true );
				}
				embedPlayer.triggerHelper( 'liveStreamStatusUpdate', { 'onAirStatus' : true } );
				return;
			}

			var service = 'liveStream';
			//type liveChannel
			if ( embedPlayer.kalturaPlayerMetaData && embedPlayer.kalturaPlayerMetaData.type == 8 ) {
				service = 'liveChannel';
			}
			var protocol = 'hls';
			if ( embedPlayer.streamerType != 'http' ) {
				protocol = embedPlayer.streamerType;
			}

            var requestObj = {
                'service' : service,
                'action' : 'islive',
                'id' : embedPlayer.kentryid,
                'protocol' : protocol,
                'partnerId': embedPlayer.kpartnerid
            };
            if ( mw.isIOSAbove7() ) {
                requestObj.rnd = Math.random();
            }
			_this.getKalturaClient().doRequest( requestObj, function( data ) {
				var onAirStatus = false;
				if ( data === true ) {
					onAirStatus = true;
                    _this.removeLiveStreamStatusMonitor();
                } else if (data === false) {
                    // now since the player is offline, need to set
                    // the live live monitor's interval to 30 seconds
                    // and do nothing -- at this point waiting
                    // for the player to come back online
                }
				if ( callback ) {
					callback( onAirStatus );
				}
				embedPlayer.triggerHelper( 'liveStreamStatusUpdate', { 'onAirStatus' : onAirStatus } );
			},mw.getConfig("SkipKSOnIsLiveRequest"),function(){
				mw.log("Error occur while trying to check onAir status");
				embedPlayer.triggerHelper( 'liveStreamStatusUpdate', { 'onAirStatus' : false } );
			} );
		},

		getKalturaClient: function() {
			if( ! this.kClient ) {
				this.kClient = mw.kApiGetPartnerClient( this.embedPlayer.kwidgetid );
			}
			return this.kClient;
		},

		log: function( msg ) {
			mw.log( "LiveStream :: " + msg);
		},

		isNativeHLS: function() {
			if ( mw.isIOS() || mw.isDesktopSafari() || mw.isAndroid() ) {
				return true;
			}
			return false;
		},

        calculateOfflineAlertOffest: function() {
            var offlineAlertOffest = this.getConfig( 'offlineAlertOffest' );
            var bufferLength = this.getPlayer().getCurrentBufferLength() *1000;
            if(bufferLength>0){
                offlineAlertOffest = bufferLength;
            }
            return offlineAlertOffest;
        }

	}));

} )( window.mw, window.jQuery );
