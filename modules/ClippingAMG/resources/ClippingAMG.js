(function (mw, $) {
    "use strict";

    mw.PluginManager.add('clippingAMG', mw.KBaseComponent.extend({

        defaultConfig: {
            segmentsPackage: null,
            playFrom: null,
            debug: false
        },
        getBaseConfig: function () {
            var parentConfig = this._super();
            return $.extend({}, parentConfig, {});
        },

        version: "1.0.8",
        segments: null,
        calculationState:false,
        preventSeek:true,
        embedPlayer: null,
        totalDurationOriginalVideo:-1,
        videoDuration: 0,
        firstPlay: true,
        $scope: {},

        iconBtnClass: "icon-debug-info",
        /**
         * setup plugin
         */
        setup: function () {
            var _this = this;
            var segments = this.getConfig('segmentsPackage');
            
            if( ! segments ){ return; }
        
            _this.embedPlayer = _this.getPlayer();
            this.trace("version: ", this.version);

            this.bind("durationChange",function(event,data){

               _this.videoDuration = data;
                _this.trace("durationChanged__", data);
                _this.totalDurationOriginalVideo = data;

                //
                // if last segment value is -1 it means we want to play video to the end
                //
                if(_this.segments[_this.segments.length -1][1] == -1){
                    _this.segments[_this.segments.length -1][1] = _this.totalDurationOriginalVideo;
                }

                _this.embedPlayer.sendNotification("clippingSegments", _this.segments);

                if(_this.segments){
                    _this.embedPlayer.sendNotification("durationWithoutOmitSegments", _this.getTotalTime());
                }
            });

        if (segments && segments.length > 0) {

                var currentSegments;
                currentSegments = segments.slice(1,segments.length-1);
                currentSegments = JSON.parse("[" + currentSegments + "]");

                // overlay logic doesnt work on android
                // if( currentSegments[0][0] != '0' && !mw.getConfig('useHlsJS')){
                //     if (mw.getConfig('autoPlay')) {
                //         _this.embedPlayer.getVideoHolder().append("<div id='" + this.embedPlayer.id + "_overlay' style='background:#000000; position:absolute; width:100%; height:100%'></div>");
                //     } else {
                //         this.embedPlayer.addJsListener('firstPlay', function () {
                //             _this.embedPlayer.getVideoHolder().append("<div id='" + this.id + "_overlay' style='background:#000000; position:absolute; width:100%; height:100%'></div>");
                //         });
                //     }
                // }

            var mediaPlayFrom = this.getConfig('playFrom') ? this.getConfig('playFrom') : currentSegments[0][0];
            this.embedPlayer.setFlashvars('mediaProxy.mediaPlayFrom', mediaPlayFrom);
            }
            this.embedPlayer.addJsListener('playerSeekEnd', function () {
                _this.preventSeek = false;
                var playerID = this.id;
                setTimeout(function () {
                    $("#" + playerID + '_overlay').remove();
                }, 100);
            });

            this.embedPlayer.addJsListener('mediaReady', function () {
                _this.preventSeek = false;
            });

            
            _this.trace("segments_received", segments);
            segments = segments.slice(1,segments.length-1);
            segments = JSON.parse("[" + segments + "]");
            _this.trace("segments_parsed", segments);
            if(segments){
                _this.segments = segments;
                _this.bindTimeUpdate();
                if(_this.videoDuration != 0){
                    _this.embedPlayer.sendNotification("durationWithoutOmitSegments", _this.getTotalTime());
                }
            }
            else {
                _this.trace("no segment file specified")
            }
        },

        /**
         * function calculate time of the video after clipping
         * @returns {number}
         */
        getTotalTime: function () {
            var total = 0;
            for (var i = 0; i < this.segments.length; i++) {
                total +=  Number(this.segments[i][1]) - Number(this.segments[i][0]);
            }
            return total;
        },

        /**
         * bind time update
         */
        bindTimeUpdate: function () {
            var _this = this;
            this.bind("timeupdate",function () {
                if (!_this.getPlayer().isInSequence() && _this.preventSeek == false){
                    var currentTime = _this.embedPlayer.evaluate('{video.player.currentTime}');

                    _this.trace("this.embedPlayer.evaluate('{video.player.currentTime}') = ", _this.embedPlayer.evaluate('{video.player.currentTime}'));
                    var isInSegment = _this.checkifInSegment(currentTime);
                    _this.trace("inSegment = ", isInSegment);
                    if (!isInSegment) {
                        _this.goToNextSegment(currentTime);
                    }
                    var newCurrentTime = _this.timeWithNoSegments(currentTime);
                    if(currentTime > _this.segments[_this.segments.length -1][1]){
                        newCurrentTime = 0;
                    }
                    var percentage = newCurrentTime / _this.getTotalTime();

                    _this.trace("newCurrentTime = ", newCurrentTime);
                    _this.trace("percentage = ", percentage);

                    _this.embedPlayer.sendNotification("timeupdate2", newCurrentTime);
                    _this.embedPlayer.sendNotification("updatePlayHeadPercent2", percentage);

                }
            });
        },

        /**
         * calculate time without segments
         * @param timeMoment
         * @returns {string}
         */
        timeWithNoSegments: function (timeMoment) {
            var additionalSegments = this.getAllAdditionalTimeSegments(timeMoment);
            var cutTimeSegmentsLength = 0;
            var prevBlockMax = 0;
            for (var i = 0; i < additionalSegments.length; i++) {
                cutTimeSegmentsLength += additionalSegments[i][0] - prevBlockMax;
                prevBlockMax = additionalSegments[i][1];
            }
            var returnTime = timeMoment - cutTimeSegmentsLength;
            return returnTime.toString();
        },
        /**
         * get list off all time segments
         * @param time
         * @returns {Array}
         */
        getAllAdditionalTimeSegments: function (time) {
            var _this = this;
            var segmentsWithTimeToAdd = [];
            for (var i = 0; i < _this.segments.length; i++) {
                if (time > _this.segments[i][0] || (time >= _this.segments[i][0] && time <= _this.segments[i][1])) {
                    segmentsWithTimeToAdd.push(_this.segments[i]);
                }
            }
            return segmentsWithTimeToAdd;
        },

        /**
         * check if video is in allowed segment
         * @returns {boolean}
         */
        checkifInSegment: function (currentTime) {
            var _this = this;
            for (var i = 0; i < _this.segments.length; i++) {
                if (currentTime >= _this.segments[i][0] && currentTime <= _this.segments[i][1]) {
                    return true;
                }
            }
            return false;
        },

        /**
         * jump to next allowed segment
         * @param currentTime
         */
        goToNextSegment: function (currentTime) {
            var _this = this;
            if(_this.preventSeek || this.embedPlayer.getPlayerElement().readyState != 4)
                return;
            for (var i = 0; i < _this.segments.length; i++) {
                if (currentTime < _this.segments[i][0]) {
                    var segNumber = i;
                    _this.calculationState = true;
                    if (Math.floor(currentTime) == 0){

                        setTimeout(function(){
                            // Edge and IE is too slow with hls.loadMedia to be able to seek right after starting playing
                            // delay for seeking has to be introduced not to crash the player
                            // change can be apply for all cases just in case hls.loadMedia would be slow on any other browser or device
                            if(!mw.getConfig('useHlsJS')){
                                var seekTo = _this.firstPlay && _this.getConfig('playFrom') ? _this.getConfig('playFrom') : _this.segments[segNumber][0];
                                _this.firstPlay = false;
                                _this.preventSeek = true;
                                _this.embedPlayer.seek(seekTo);
                            }
                        },200);
                        return;
                    }
                    else{
                        _this.preventSeek = true;
                        _this.embedPlayer.seek(_this.segments[i][0]);
                        return;
                    }
                }
            }
            _this.preventSeek = true;
            _this.embedPlayer.seek(_this.segments[0][0],true);
        },

        /**
         * trace plugin if debug option is true
         * @param str
         */
        trace: function (str, var1) {
            if (this.getConfig('debug')) {
                console.log("___clippingPlugin ", str, var1);
            }
        }


    }));

})(window.mw, window.jQuery);